# WEB vláčky

## Project development setup
```
cp ./docker/development/docker-compose.example.yml ./docker-compose.yml
```

### With docker

For local use of ssh key:

```
cp ./server/src/config/config.example.json ./server/src/config/config.json
```

```
eval `ssh-agent`
ssh-add ~/.ssh/id_rsa
```

```
docker-compose build
docker-compose run trains_webserver bash -c "npm i"
```

Run containers:
```
docker-compose up
```

Connect to development container console:
```
docker exec -it trains_webserver bash
```

To use matomo add to /var/www/config/config.ini
```
trusted_hosts[] = "localhost:8088"
trusted_hosts[] = "trains_matomo:80"
proxy_uri_header = 1
proxy_client_headers[] = HTTP_X_FORWARDED_FOR
proxy_host_headers[] = HTTP_X_FORWARDED_HOST
```

To run container, connect to it and use local ssh you can use script up.sh

Client app is running on port 8080
Server app is running on port 8081 or localhost:8080/api

Adminer is on port 8089 or localhost:8080/adminer and login is:
server: trains_db
username: web_trains
password: test1234
db: web_trains

Matomo is on port 8088 or localhost:8080/analytics

### Without docker
```
npm i -g npm 
npm i -g nodemon @vue/cli@^5.0.0-beta.0
npm i -g @quasar/icongenie --unsafe-perm
npm install
npm run serve
```

Client app is running on port 8080
Server app is running on port 8081

In case of need, you have to install Matomo and Adminer manually.

## Project development

Compiles and minifies for production:
```
npm run build
```

Lints and fixes files:
```
npm run lint
```

## Development - server
Create new database migration:
```
npx knex migrate:make name
```
Run new migrations:
```
npx knex migrate:latest
```
Add database data:
```
npx knex seed:run
```

## Development - client

### Generate icons
```
icongenie generate -m spa -i src/assets/icon-train-blue.png
```

## Production testing - docker
Generate dev ssl certificat
docker/production/cert

```
openssl req -x509 -out web_trains.crt -keyout web_trains.key \
  -newkey rsa:2048 -nodes -sha256 \
  -subj '/CN=localhost' -extensions EXT -config <( \
   printf "[dn]\nCN=localhost\n[req]\ndistinguished_name = dn\n[EXT]\nsubjectAltName=DNS:localhost\nkeyUsage=digitalSignature\nextendedKeyUsage=serverAuth")
```

In folder dist must be builded files

```
cp ./docker/production/docker-compose.example.yml ./docker-compose.prod.yml
docker-compose -f docker-compose.prod.yml up
```

Connect to production testing container console:

```
docker exec -it trains_prod_webserver bash
```

To run container, connect to it you can use script up.sh prod

app run on port 80

Matomo: localhost/analytics
Adminer: localhost/adminer

## Production setup

create mysql credentials

db: web_trains
user: web_trains
password: test1234

db: matomo
user: matomo
password: test1234

```
mysql_secure_installation

mysql

CREATE USER 'web_trains'@'localhost' IDENTIFIED BY 'test1234';
CREATE DATABASE web_trains;
GRANT ALL PRIVILEGES ON web_trains . * TO 'web_trains'@'localhost';
CREATE USER 'matomo'@'localhost' IDENTIFIED BY 'test1234';
CREATE DATABASE matomo;
GRANT ALL PRIVILEGES ON matomo . * TO 'matomo'@'localhost';
FLUSH PRIVILEGES;
```

Monitore status
```
pm2 monit
```

Renew certificates with [certbot](https://certbot.eff.org/)

## Production deployment


## Configuration

Location of config.json depends on enviroment:

- Production - /config
- Development - /server/app/config

```
"db": {
	"host": "trains_db",
	"user": "web_trains",
	"password": "test1234",
	"database": "web_trains"
},
"app": {
	"port": 8081,
	"listen": "0.0.0.0",
	"secret": "oijfjo45",
	"mode": "production"
},
"hardware": {
	"port": 8083,
	"host": "false"
}
```
### db
- host - adress of database host
- user - database user name
- password - dadtabase password
- database - db name

### app
- port - port on which server - api, socket runs
- listen - adress for server
- secret - used for generating JWT
- mode:
  - production - logs are stored in files
  - development - logs are send to console
  - env - depends on runnig context

### hardware
- port app train
- host app train. For disallow connection set false
## SVG map comments
Tratě:
    trat1a
	trat1b
	trat1c
	trat2a
	trat2b
	trat2c
	trat3a
	trat3b
	trat4

Stanice:
    beroun13
    beroun11
    beroun11a
    beroun9a
    beroun9
    beroun9b
    beroun9c
	beroun15
    beroun7
    beroun5
    beroun3
    beroun1
    beroun2a
    beroun2b
    beroun4a
    beroun4b
    beroun6
    beroun8

	karlstejn7
	karlstejn7a
	karlstejn5
	karlstejn3
	karlstejn1
	karlstejn2
	karlstejn4a
	karlstejn4b
	karlstejn6

	lhota5
	lhota3
	lhota3a
	lhota1
	lhota1a
	lhota2
	lhota2a
	lhota2b
