#!/bin/sh
rm -rf node_modules
rm -rf package-lock.json
rm -rf client/node_modules
rm -rf client/package-lock.json
rm -rf server/node_modules
rm -rf server/package-lock.json
rm -rf types/node_modules
rm -rf types/package-lock.json

npm i
npm update