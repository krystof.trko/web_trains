import { createServer } from 'http'
import mount from 'koa-mount'

import ApiBootstrap from './app/ApiBootstrap'
import ClientBootstrap from './app/ClientBootstrap'
import Configuration from './app/config/Configuration'
import HwBootstrap from './app/HwClientBootstrap'
import SocketBootstrap from './app/SocketBootstrap'

const conf = new Configuration()

// api section
const api = (new ApiBootstrap(conf)).run()
let server

// code removed by weback on production
if (process.env.NODE_ENV === 'development') {
  // vue app section - in production mode is used nginx
  const client = (new ClientBootstrap(conf)).run()
  client.use(mount(Configuration.ADRESS_API, api))
  server = client
} else {
  api.use(mount(Configuration.ADRESS_API, api))
  server = api
}

const httpServer = createServer(server.callback())

// Hardware communication
const hwSocket = new HwBootstrap(conf).run()

// socket section app
new SocketBootstrap(conf, hwSocket, httpServer).run()

httpServer.listen(conf.app.port)
