import fs from 'fs'
import { Validator } from 'jsonschema'
import path from 'path'

interface App {
  port: number
  listen: string
  secret: string
  mode: string
}

interface Hardware {
  port: number
  host: string
}

/**
 * Configuration from json file and for directory
 */
export default class Configuration {
  static readonly MODE_PROD = 'production'
  static readonly MODE_DEV = 'development'
  static readonly MODE_ENV = 'env'

  static readonly PATH_LOG = 'log/web_trains/'
  static readonly PATH_CLIENT = 'client/'
  static readonly PATH_CONFIG = 'config/'

  static readonly ADRESS_API = '/api'
  static readonly ADRESS_SOCKET = '/socket'

  public db: any
  public app: App
  public hardware: Hardware
  public mode: string // mode from config.json - for logs, ...
  public envMode: string // mode from enviroment for dir paths, ...

  constructor () {
    // undefined means it is not bounded by webpack
    this.envMode = process.env.NODE_ENV === undefined ? 'development' : process.env.NODE_ENV

    this.checkDir(Configuration.PATH_LOG)
    this.checkDir(Configuration.PATH_CLIENT)
    this.checkDir(Configuration.PATH_CONFIG)

    const config = this.readConfig()

    this.db = {
      client: 'mysql2',
      connection: config.db,
      migrations: {
        tableName: 'migrations'
      }
    }

    this.app = config.app
    this.hardware = config.hardware

    this.mode = this.app.mode === Configuration.MODE_ENV ? this.envMode : this.app.mode
  }

  /**
   * Is production mode config
   * @returns boolean
   */
  public isProduction (): boolean {
    return this.mode === Configuration.MODE_PROD
  }

  /**
   * Is dev mode in config
   * @returns boolean
   */
  public isDevelopment (): boolean {
    return this.mode === Configuration.MODE_DEV
  }

  /**
   * Get directory path
   * root dir depends on process.env
   * @param dir
   * @returns
   */
  public path (dir = ''): string {
    if (this.envMode === Configuration.MODE_DEV) {
      return path.join(__dirname, '/../../', dir)
    }
    return path.join(__dirname, dir)
  }

  /**
   * Create dir if not exist on startup
   * @param dir
   */
  private checkDir (dir: string): void {
    fs.mkdir(
      this.path(dir),
      { recursive: true },
      (err: any) => {
        if (err !== null && err.code !== 'EEXIST') {
          throw new Error('Could not create directory: ' + dir + err.message)
        }
      })
  }

  /**
   * Read json config file
   * @returns any
   */
  private readConfig (): any {
    const path = this.path(Configuration.PATH_CONFIG) + 'config.json'
    const data = fs.readFileSync(path, 'utf8')
    const config = JSON.parse(data)

    this.validate(config)

    return config
  }

  /**
   * Validate json config file
   * @param config any
   */
  private validate (config: any): void {
    const valid = new Validator()

    const dbSchema = {
      id: '/DbSchema',
      type: 'object',
      properties: {
        host: { type: 'string' },
        user: { type: 'string' },
        password: { type: 'string' },
        database: { type: 'string' }
      },
      required: ['host', 'user', 'password', 'database']
    }

    const appSchema = {
      id: '/AppSchema',
      type: 'object',
      properties: {
        port: { type: 'number' },
        listen: { type: 'string' },
        secret: { type: 'string' },
        mode: {
          enum: [
            Configuration.MODE_DEV, Configuration.MODE_PROD, Configuration.MODE_ENV
          ]
        }
      },
      required: ['port', 'listen']
    }

    const hardwareSchema = {
      id: '/HardwareSchema',
      type: 'object',
      properties: {
        port: { type: 'number' },
        host: { type: 'string' }
      },
      required: ['port', 'host']
    }

    const schema = {
      id: '/Schema',
      type: 'object',
      properties: {
        db: { $ref: '/DbSchema' },
        app: { $ref: '/AppSchema' },
        hardware: { $ref: '/HardwareSchema' }
      },
      required: ['db', 'app', 'hardware']
    }

    valid.addSchema(dbSchema, '/DbSchema')
    valid.addSchema(appSchema, '/AppSchema')
    valid.addSchema(hardwareSchema, '/HardwareSchema')

    const result = valid.validate(config, schema)

    if (!result.valid) {
      throw Error('config.json error:\n' + result.toString())
    }
  }
}
