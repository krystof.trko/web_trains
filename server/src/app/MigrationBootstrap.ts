import Knex from 'knex'
import { Model } from 'objection'

import Configuration from './config/Configuration'
import UserService from './service/UserService'

export default class MigrationBootstrap {
  public run (): void {
    const config = new Configuration()
    const knex = Knex(config.db)
    Model.knex(knex)
    UserService.setSecret(config.app.secret)
  }
}
