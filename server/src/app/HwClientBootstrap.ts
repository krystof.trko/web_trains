import { Socket } from 'net'

import Configuration from './config/Configuration'
import { log } from './exception/Logger'
import HardwareTcp from './hardware/HardwareTcp'

export default class HwSocketBootstrap {
  private hwSocket: Socket

  constructor (private config: Configuration) {
    this.hwSocket = new Socket()
    this.hwSocket.setKeepAlive(true)
  }

  public run (): HardwareTcp {
    this.hwSocket.on('ready', () => {
      this.log('Connected to hardware')
    })

    this.hwSocket.on('timeout', () => {
      this.log('Connection timeout')
    })

    this.hwSocket.on('error', (error) => {
      this.log('Hw error: ' + error)
    })

    this.hwSocket.on('close', () => {
      this.log('Closed, reconnecting ...')
      setTimeout(() => this.connect(), 10000)
    })

    this.connect()

    return new HardwareTcp(this.hwSocket)
  }

  private connect () {
    if (this.config.hardware.host === 'false') {
      return
    }

    this.hwSocket.connect(this.config.hardware.port, this.config.hardware.host)
  }

  private log (err: unknown) {
    log(this.config.isProduction(), this.config.path(Configuration.PATH_LOG), err, 'warn')
  }
}
