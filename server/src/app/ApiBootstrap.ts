import cors from '@koa/cors'
import Knex from 'knex'
import Koa from 'koa'
import bodyParser from 'koa-bodyparser'
import helmet from 'koa-helmet'
import KoaRouter from 'koa-router'
import { Model } from 'objection'
import { RolesManager } from 'web_trains_types'

import Configuration from './config/Configuration'
import CmsController from './controller/CmsController'
import DelayController from './controller/DelayController'
import ErrorController from './controller/ErrorController'
import JourneyController from './controller/JourneyController'
import PlannerController from './controller/PlannerController'
import StationController from './controller/StationController'
import TestController from './controller/TestController'
import TimetableController from './controller/TimetableController'
import TrainController from './controller/TrainController'
import UserController from './controller/UserController'
import UserSignController from './controller/UserSignController'
import errorHandler from './exception/Handler'
import { MethodNotAllowed } from './exception/http/MethodNotAllowed'
import UserService from './service/UserService'

export default class ApiBootstrap {
  private app: Koa
  private router: KoaRouter
  private model: Knex<any, unknown[]>
  private roles: RolesManager

  constructor (private config: Configuration) {
    this.router = new KoaRouter()
    this.app = new Koa()

    const knex = Knex(config.db)
    this.model = Model.knex(knex)

    this.roles = new RolesManager()

    UserService.setSecret(config.app.secret)
  }

  /**
   * Run api aplication
   * @returns Koa
   */
  public run (): Koa {
    this.app.use(errorHandler(this.config.isProduction(), this.config.path(Configuration.PATH_LOG)))
    this.setMiddleware()
    this.setControllers()
    this.setRoutes()

    return this.app
  }

  /**
   * Add controller routes
   */
  private setControllers (): void {
    new CmsController(this.router, this.roles, this.config).setRouter()
    new DelayController(this.router, this.roles, this.config).setRouter()
    new ErrorController(this.router, this.roles, this.config).setRouter()
    new JourneyController(this.router, this.roles, this.config).setRouter()
    new PlannerController(this.router, this.roles, this.config).setRouter()
    new StationController(this.router, this.roles, this.config).setRouter()
    new TestController(this.router, this.roles, this.config).setRouter()
    new TimetableController(this.router, this.roles, this.config).setRouter()
    new TrainController(this.router, this.roles, this.config).setRouter()
    new UserController(this.router, this.roles, this.config).setRouter()
    new UserSignController(this.router, this.roles, this.config).setRouter()
  }

  /**
   * Add security and parse
   */
  private setMiddleware (): void {
    this.app
      .use(helmet())
      .use(cors({
        allowMethods: 'GET,POST,DELETE,PATCH'
      }))
      .use(bodyParser())
  }

  /**
   * Add routes
   */
  private setRoutes (): void {
    this.app
      .use(this.router.routes())
      .use(this.router.allowedMethods({
        throw: true,
        methodNotAllowed: () => new MethodNotAllowed()
      }))
  }
}
