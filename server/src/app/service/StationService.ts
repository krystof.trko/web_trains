import { StationModel } from '../model/StationModel'

export default class StationService {
  /**
   * All stations
   * @returns Promise<StationModel[]>
   */
  public async getAll (): Promise<StationModel[]> {
    return StationModel.query()
      .orderBy('id')
  }
}
