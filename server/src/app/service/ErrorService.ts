import { once } from 'events'
import fs from 'fs'
import readline from 'readline'
import { LogErrorRow, LogErrorType } from 'web_trains_types'

import Configuration from '../config/Configuration'

export default class ErrorService {
  constructor (private config: Configuration) {}

  /**
   * Errors from logs
   * @param type
   * @param count
   * @param offset
   * @returns LogErrorRow[]
   */
  public async getErrors (type: LogErrorType, count = 10, offset = 0): Promise<LogErrorRow[]> {
    const result:LogErrorRow[] = []

    const file = this.getFile(type)

    if (file === null) {
      return []
    }

    const rl = readline.createInterface({
      input: fs.createReadStream(file),
      terminal: false
    })

    rl.on('line', (line) => {
      if (--offset >= 1) {
        return
      }

      if (--count <= 0) {
        rl.close()
        rl.removeAllListeners()
      }

      result.push(JSON.parse(line))
    })

    await once(rl, 'close')

    return result
  }

  /**
   * Count lines in log file
   * @param type
   * @returns Promise<number>
   */
  public async getCount (type: LogErrorType): Promise<number> {
    return new Promise((resolve, reject) => {
      const file = this.getFile(type)

      if (file === null) {
        resolve(0)
        return
      }

      let lineCount = 0

      fs.createReadStream(file)
        .on('data', (buffer) => {
          let idx = -1
          lineCount--

          do {
            idx = buffer.indexOf('\n', idx + 1)
            lineCount++
          } while (idx !== -1)
        }).on('end', () => {
          resolve(lineCount)
        }).on('error', reject)
    })
  }

  private getFile (type: LogErrorType): string|null {
    const path = this.config.path(Configuration.PATH_LOG)

    if (!Object.values(LogErrorType).includes(type)) {
      return null
    }

    const file = path + type + '.log'

    if (!fs.existsSync(file)) {
      return null
    }

    return file
  }
}
