import { DelayModel as Delay, DelayPut } from 'web_trains_types'

import { DelayModel } from '../model/DelayModel'

export default class DelayService {
  /**
   * All delays
   * @returns Promise<Delay[]>
   */
  public async getAll (): Promise<Delay[]> {
    return DelayModel.query()
  }

  /**
   * One delay by train and station
   * @returns Promise<Delay[]>
   */
  public async get (trainId: number|null, stationId: number|null): Promise<Delay> {
    return DelayModel.query()
      .where({
        train_id: trainId,
        station_id: stationId
      })
      .limit(1)
      .first()
  }

  /**
   * If resource not exist add new. If exist change delay.
   * @returns Promise<number|Delay>
   */
  public async put (data: DelayPut): Promise<number|DelayModel> {
    const old = await this.get(data.trainId, data.stationId)

    if (old) {
      return DelayModel.query()
        .where({ id: old.id })
        .update({ delay: data.delay })
    }

    return DelayModel.query()
      .insert({
        trainId: data.trainId,
        stationId: data.stationId,
        delay: data.delay
      })
  }
}
