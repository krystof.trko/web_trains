
import { format } from 'date-fns'
import sanitizeHtml from 'sanitize-html'
import { CmsModel as Cms } from 'web_trains_types'

import { CmsModel } from '../model/CmsModel'

export default class CmsService {
  /**
   * One cms delete userId and join user
   * @param id
   * @returns Promise<Cms|undefined>
   */
  public async get (id: number): Promise<Cms|undefined> {
    return CmsModel.query()
      .findById(id)
      .withGraphJoined('user')
      .then((data: Cms) => {
        if (data === undefined) {
          return
        }

        delete data.userId

        return data
      })
  }

  /**
   * Update cms, set lastChanged and current userID
   * @param cms
   * @param userId
   * @returns Promise<number>
   */
  public async patch (cms: CmsModel, userId: number): Promise<number> {
    const patch = {
      text: sanitizeHtml(cms.text),
      lastChanged: format(new Date(), 'yyyy-MM-dd HH:mm:ss'),
      userId: userId
    }

    return CmsModel.query()
      .findById(cms.id)
      .patch(patch)
  }
}
