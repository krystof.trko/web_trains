import bcrypt from 'bcryptjs'
import jwt from 'jsonwebtoken'
import { Role, UserSignInRes } from 'web_trains_types'

import { UserModel } from '../model/UserModel'

export default class UserService {
  private static secret: string

  /**
   * Static set secret for jwt
   * @param secret
   */
  public static setSecret (secret: string): void {
    this.secret = secret
  }

  /**
   * All registered users
   * @returns Promise<UserModel[]>
   */
  public async getAll (): Promise<UserModel[]> {
    return UserModel.query()
  }

  /**
   * One user by username
   * @param id
   * @returns Promise<UserModel>
   */
  public async getById (id: number): Promise<UserModel> {
    return UserModel.query().findById(id)
  }

  /**
   * Hash user password and add to db and role visitor
   * @param user
   * @returns Promise<UserModel>
   */
  public async add (user: UserModel): Promise<UserModel> {
    user.password = bcrypt.hashSync(user.password, 15)
    user.role = Role.visitor

    return UserModel.query().insert(user)
  }

  /**
   * Update one user
   * @param user
   * @returns Promise<number>
   */
  public async patch (user: UserModel): Promise<number> {
    const patch = {
      password: user.password,
      name: user.name,
      role: user.role
    }

    if (patch.password !== undefined) {
      patch.password = bcrypt.hashSync(user.password, 15)
    }

    return UserModel.query()
      .findById(user.id)
      .patch(patch)
  }

  /**
   * Delete one user
   * @param userId
   * @returns Promise<number>
   */
  public async delete (userId: number): Promise<number> {
    return UserModel.query().deleteById(userId)
  }

  /**
   * Get user by name
   * @param name
   * @returns Promise<UserModel>
   */
  public async getByName (name: string): Promise<UserModel> {
    return UserModel.query().findOne({
      name: name
    })
  }

  /**
   * Check password and hash
   * @param hash
   * @param password
   * @returns boolean
   */
  public checkPassword (hash: string, password: string): boolean {
    return !bcrypt.compareSync(hash, password)
  }

  /**
   * Create JWT
   * @param user
   * @returns Promise<UserSignInRes>
   */
  public async signIn (user: UserModel): Promise<UserSignInRes> {
    const payload = {
      id: user.id
    }

    const token = jwt.sign(
      payload,
      UserService.secret,
      { expiresIn: '1w' }
    )

    return {
      token: token
    }
  }
}
