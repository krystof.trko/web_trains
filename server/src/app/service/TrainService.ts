import Objection from 'objection'
import { TrainPosition } from 'web_trains_types'

import { TrainModel } from '../model/TrainModel'

export default class TrainService {
  /**
   * All trains
   * @returns Promise<TrainModel[]>
   */
  public async getAll (onlyWithPos: boolean): Promise<TrainModel[]> {
    const q = TrainModel.query()
      .orderBy('id')

    if (onlyWithPos) {
      q.whereRaw('position_id IS NOT NULL')
    }

    return q
  }

  /**
   * Current train positions
   * @returns Promise<TrainPosition[]>
   */
  public async getPositions (): Promise<TrainPosition[]> {
    return <Promise<TrainPosition[]>><unknown>TrainModel.query()
      .select('id as t', 'position_id as p')
      .whereRaw('position_id IS NOT NULL')
  }

  /**
   * Patch lights
   * @param id
   * @param light
   * @returns Promise<number>
   */
  public async setLight (id: number, light: boolean): Promise<number> {
    return this.patch(id, {
      light: light
    })
  }

  /**
   * Patch position
   * @param id
   * @param positionId
   * @returns Promise<number>
   */
  public async setPosition (id: number, positionId: number): Promise<number> {
    return this.patch(id, {
      positionId: positionId
    })
  }

  /**
   * Patch speed
   * @param id
   * @param positionId
   * @returns Promise<number>
   */
  public async setSpeed (id: number, speed: number): Promise<number> {
    return this.patch(id, {
      speed: speed
    })
  }

  /**
   * Patch anything
   * @param id
   * @param data
   * @returns Promise<number>
   */
  private async patch (id: number, data: Objection.PartialModelObject<TrainModel>): Promise<number> {
    return TrainModel.query()
      .patch(data)
      .where({
        id: id
      })
  }
}
