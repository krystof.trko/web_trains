import { JourneyAdd, JourneyModel as Journey, JourneyPatch } from 'web_trains_types'

import { JourneyModel } from '../model/JourneyModel'

export default class JourneyService {
  /**
   * One journey with stations, train and sections
   * @param id
   * @returns Promise<Journey|undefined>
   */
  public async get (id: number): Promise<Journey|undefined> {
    return JourneyModel.query()
      .findById(id)
      .withGraphFetched('[stationFrom, stationTo, train, section]')
  }

  /**
   * All journeys with train
   * @returns Promise<Journey[]>
   */
  public async getAll (): Promise<Journey[]> {
    return JourneyModel.query()
      .withGraphFetched('[stationFrom, stationTo, train]')
  }

  /**
   * All currently running journeys
   * @returns Promise<Journey[]>
   */
  public async getAllRunning (): Promise<Journey[]> {
    return JourneyModel.query()
      .where({ running: true })
      .withGraphFetched('[stationFrom, stationTo, train]')
  }

  /**
   * Run journey
   * @param id
   * @returns Promise<number>
   */
  public async run (id: number): Promise<number> {
    return JourneyModel.query()
      .findById(id)
      .patch({
        running: true
      })
  }

  /**
   * Stop journey
   * @param id
   * @returns Promise<number>
   */
  public async stop (id: number): Promise<number> {
    return JourneyModel.query()
      .findById(id)
      .patch({
        running: false
      })
  }

  /**
   * Add journey with id relations
   * @param data
   * @returns Promise<JourneyModel>
   */
  public async add (data: JourneyAdd): Promise<JourneyModel> {
    const journey = await JourneyModel.query().insert({
      stationFromId: data.stationFromId,
      stationToId: data.stationToId,
      trainId: data.trainId,
      speed: data.speed,
      direction: data.direction
    })

    if (data.section !== undefined) {
      await this.addSectionRelation(journey.id, data.section)
    }

    return journey
  }

  /**
   * Delete journey with sections by journey id
   * @param id
   * @returns Promise<number>
   */
  public async delete (id: number): Promise<number> {
    let affected = 0

    affected += await this.deleteSectionRelation(id)
    affected += await JourneyModel.query().deleteById(id)

    return affected
  }

  /**
   * Update journey
   * to delete section = []
   * @param id
   * @param data
   * @returns Promise<number>
   */
  public async patch (id: number, data: JourneyPatch): Promise<number> {
    let affected = 0

    if (data.section !== undefined) {
      affected += await this.deleteSectionRelation(id)
      affected += await this.addSectionRelation(id, data.section)
    }

    if (data.stationFromId !== undefined) {
      affected += await JourneyModel.query()
        .findById(id)
        .patch({
          stationFromId: data.stationFromId,
          stationToId: data.stationToId,
          trainId: data.trainId,
          speed: data.speed,
          direction: data.direction
        })
    }

    return affected
  }

  /**
   * Delete all related from journey_section
   * @param journeyId
   * @returns Promise<number>
   */
  private async deleteSectionRelation (journeyId: number): Promise<number> {
    return JourneyModel.relatedQuery('section').for(journeyId).unrelate()
  }

  /**
   * Add relate to journey_section
   * @param journeyId
   * @param sectionAll
   * @returns Promise<number>
   */
  private async addSectionRelation (journeyId: number, sectionAll: Array<number>): Promise<number> {
    for (const sectionId of sectionAll) {
      await JourneyModel.relatedQuery('section')
        .for(journeyId)
        .relate(sectionId)
    }

    return sectionAll.length
  }
}
