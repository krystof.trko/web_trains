import { TimetableAdd, TimetableModel as Timetable, TimetablePatch } from 'web_trains_types'

import { TimetableModel } from '../model/TimetableModel'

export default class TimetableService {
  /**
   * Timetable with all journeys
   * Journey with train, stationFrom and stationTo (no section)
   * @param id
   * @returns Promise<Timetable|undefined>
   */
  public async get (id: number): Promise<Timetable|undefined> {
    return TimetableModel.query()
      .findById(id)
      .withGraphFetched('journey.[train, stationFrom, stationTo]')
  }

  /**
   * All timetabes without journeys
   * @returns Promise<Timetable[]>
   */
  public async getAll (): Promise<Timetable[]> {
    return TimetableModel.query()
  }

  /**
   * Add new timetable and timetable_journey
   * @param data
   * @returns Promise<TimetableModel>
   */
  public async add (data: TimetableAdd): Promise<TimetableModel> {
    const timetable = await TimetableModel.query().insert({
      name: data.name,
      sequence: data.sequence
    })

    await this.addJourneyRelation(timetable.id, data.journey)

    return timetable
  }

  /**
   * Delete from timetable and timetable_journey
   * @param id
   * @returns Promise<number>
   */
  public async delete (id: number): Promise<number> {
    let affected = 0

    affected += await this.deleteJourneyRelation(id)
    affected += await TimetableModel.query().deleteById(id)

    return affected
  }

  /**
   * Change journey, journet order or name
   * to delete journey = []
   * @param data
   * @returns Promise<number>
   */
  public async patch (id: number, data: TimetablePatch): Promise<number> {
    let affected = 0

    if (data.journey !== undefined) {
      affected += await this.deleteJourneyRelation(id)
      affected += await this.addJourneyRelation(id, data.journey)
    }

    if (data.name !== undefined) {
      affected += await TimetableModel.query()
        .findById(id)
        .patch({
          name: data.name,
          sequence: data.sequence
        })
    }

    return affected
  }

  /**
   * Delete all related from timetable_journey
   * @param timetableId
   * @returns Promise<number>
   */
  private async deleteJourneyRelation (timetableId: number): Promise<number> {
    return TimetableModel.relatedQuery('journey').for(timetableId).unrelate()
  }

  /**
   * Add related to timetable_journey
   * @param timetableId
   * @param journeyAll
   * @returns Promise<number>
   */
  private async addJourneyRelation (timetableId: number, journeyAll: Array<number>): Promise<number> {
    for (const journeyId of journeyAll) {
      await TimetableModel.relatedQuery('journey')
        .for(timetableId)
        .relate(journeyId)
    }

    return journeyAll.length
  }
}
