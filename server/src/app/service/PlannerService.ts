import { format, parseISO } from 'date-fns'
import { PlannerAdd, PlannerModel as Planner, PlannerPatch, TimetableModel as Timetable } from 'web_trains_types'

import { getRange } from '../helper/PlannerHelper'
import { PlannerModel } from '../model/PlannerModel'

export default class PlannerService {
  /**
   * One planner with timetable
   * @param id
   * @returns Promise<Planner|undefined>
   */
  public async get (id: number): Promise<Planner|undefined> {
    return PlannerModel.query()
      .findById(id)
      .withGraphFetched('timetable')
      .then((data: Planner) => {
        if (data === undefined) {
          return
        }

        delete data.timetableId

        return data
      })
  }

  /**
   * All planners
   * @returns Promise<Planner[]>
   */
  public async getAll (): Promise<Planner[]> {
    return PlannerModel.query()
      .withGraphFetched('timetable')
  }

  /**
   * Add planner runned once or repetive
   * @param data
   * @returns Promise<PlannerModel>
   */
  public async add (data: PlannerAdd): Promise<PlannerModel> {
    const planner = await PlannerModel.query().insert({
      ...this.getValidData(data),
      timetableId: data.timetableId
    })

    return planner
  }

  /**
   * Delete one
   * @param id
   * @returns Promise<number>
   */
  public async delete (id: number): Promise<number> {
    return await PlannerModel.query().deleteById(id)
  }

  /**
   * Update planner runned once or repetive
   * @param id
   * @param data
   * @returns Promise<number>
   */
  public async patch (id: number, data: PlannerPatch): Promise<number> {
    return PlannerModel.query()
      .findById(id)
      .patch({
        ...this.getValidData(data),
        timetableId: data.timetableId
      })
  }

  public async getToday (): Promise<PlannerModel[]> {
    const all = await PlannerModel.query()

    return all
  }

  /**
   * Get planned planners in datetime range with timetable and journey
   * @param fromStr
   * @param toStr
   * @returns Promise<Planner[]>
   */
  public async getRange (fromStr: string, toStr: string): Promise<Planner[]> {
    const from = parseISO(fromStr)
    const to = parseISO(toStr)

    const all = await PlannerModel.query()
      .withGraphFetched('timetable.journey.[train, stationFrom, stationTo]')

    return getRange(all, from, to)
  }

  /**
   * Get all timatables for selected date
   * @param date
   * @returns Promise<Timetable[]>
   */
  public async getForDate (date: Date): Promise<Timetable[]> {
    const time = JSON.stringify({
      h: date.getHours(),
      m: date.getMinutes()
    })
    const day = date.getDay().toString()
    const month = date.getMonth().toString()

    const plannerAll = await PlannerModel.query()
      .withGraphFetched('timetable.journey.[train, stationFrom, stationTo, section]')
      .whereRaw('INSTR(time, ?) > 0 AND JSON_CONTAINS(day, ?) AND JSON_CONTAINS(month, ?)', [time, day, month])
      .orWhere({ once: format(date, 'yyyy-MM-dd HH:mm') }) as Planner[]

    const timetableAll: Timetable[] = []

    for (const planner of plannerAll) {
      if (!planner.timetable) {
        continue
      }

      timetableAll.push(planner.timetable)
    }

    return timetableAll
  }

  /**
   * Ensure consistent data
   * @param data
   * @returns PlannerPatch
   */
  private getValidData (data: PlannerPatch): PlannerPatch {
    let insert = {}

    if (data.once === undefined || data.once === null) {
      insert = {
        time: data.time,
        day: data.day,
        month: data.month,
        once: null
      }
    } else {
      insert = {
        time: null,
        day: null,
        month: null,
        once: format(parseISO(<string>data.once), 'yyyy-MM-dd HH:mm')
      }
    }

    return insert
  }
}
