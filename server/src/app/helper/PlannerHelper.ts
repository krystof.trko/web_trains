import { compareAsc, eachDayOfInterval, isWithinInterval as isWithinIntervalFnc } from 'date-fns'
import { PlannerModel as Planner, PlannerTime } from 'web_trains_types'

/**
 * Create all days in interval and pass all models to periodicToDate
 * Resolution is per days
 * @param plannerAll
 * @param from
 * @param to
 * @returns Planner[]
 */
export function getRange (plannerAll: Planner[], from: Date, to: Date): Planner[] {
  if (compareAsc(from, to) > 0) {
    const fromTemp = from
    from = to
    to = fromTemp
  }

  const allDays = eachDayOfInterval({
    start: from, end: to
  })

  const allDate = plannerAll.flatMap((planner) => periodicToDate(planner, allDays, from, to))

  return allDate.sort((a, b) => compareAsc(<Date>a.once, <Date>b.once))
}

/**
 * Go through days in interval and return suitable for cron
 * @param planner
 * @param days
 * @returns Planner[]
 */
function periodicToDate (planner: Planner, days: Date[], from: Date, to: Date): Planner[] {
  if (planner.once !== null && isWithinInterval(planner.once, from, to)) {
    return [planner]
  }

  const planners:Planner[] = []

  for (const day of days) {
    if (!planner.month?.includes(day.getMonth())) {
      continue
    }

    if (!planner.day?.includes(day.getDay())) {
      continue
    }

    planner.time?.forEach((time: PlannerTime) => {
      const newDay = new Date(day)

      newDay.setHours(time.h)
      newDay.setMinutes(time.m)

      if (!isWithinInterval(newDay, from, to)) {
        return
      }

      const newPlanner = { ...planner }
      newPlanner.once = newDay

      planners.push(newPlanner)
    })
  }

  return planners
}

function isWithinInterval (once: string|Date, from: Date, to: Date) {
  return isWithinIntervalFnc(<Date>once, {
    start: from, end: to
  })
}
