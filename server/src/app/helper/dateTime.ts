import { format } from 'date-fns'

export function dbDateTimeNow (): string {
  return format(new Date(), 'yyyy-MM-dd HH:mm:ss')
}
