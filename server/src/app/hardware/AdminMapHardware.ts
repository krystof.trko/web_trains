import { Action, events, eventTransfer, JourneyDirection, Resource, TrainFunction, TrainMotion } from 'web_trains_types'

import { NotFound } from '../exception/http/NotFound'
import JourneyService from '../service/JourneyService'
import TimetableService from '../service/TimetableService'
import TrainService from '../service/TrainService'
import BaseAuthHardware from './BaseAuthHardware'

export class AdminMapHardware extends BaseAuthHardware {
  private trainService = new TrainService()
  private journeyService = new JourneyService()
  private timetableService = new TimetableService()

  public listen (): void {
    this.setResourceAction(Resource.hardwareAdmin, Action.crud)

    this.io.on(events.trainFunction, this.onTrainFunction.bind(this))
    this.io.on(events.trainMotion, this.onTrainMotion.bind(this))

    this.io.on(eventTransfer.runJourney, this.onRunJourney.bind(this))
    this.io.on(eventTransfer.runTimetable, this.onRunTimetable.bind(this))
  }

  /**
   * Turn on/off lights
   * If hw request failed db is not changed
   * @param data
   * @param done
   * @returns void
   */
  private async onTrainFunction (data: TrainFunction, done: (success: boolean) => void): Promise<void> {
    const valid = this.validate(data, {
      properties: {
        locomotive: { type: 'string' },
        lights: { type: 'boolean' },
        trainId: { type: 'integer' }
      },
      required: ['locomotive', 'lights', 'trainId']
    })

    const hwResult = this.sendPacket(events.trainFunction, data)

    if (!hwResult || !data.trainId || !valid) {
      done(false)
      return
    }

    const dbresult = await this.trainService.setLight(data.trainId, data.lights)

    if (dbresult === 0) {
      this.exception(new NotFound())
      done(false)
      return
    }

    done(true)
  }

  /**
   * Move with train
   * @param data
   * @param done
   * @returns
   */
  private async onTrainMotion (data: TrainMotion, done: (success: boolean) => void): Promise<void> {
    const valid = this.validate(data, {
      properties: {
        locomotive: { type: 'string' },
        speed: { type: 'integer', minimum: -30, maximum: 31 },
        reverse: { enum: [JourneyDirection.ahead, JourneyDirection.reverse] },
        trainId: { type: 'integer' }
      },
      required: ['locomotive', 'speed', 'reverse', 'trainId']
    })

    const hwResult = this.sendPacket(events.trainMotion, data)

    if (!hwResult || !data.trainId || !valid) {
      done(false)
      return
    }

    const dbresult = await this.trainService.setSpeed(data.trainId, data.speed)

    if (dbresult === 0) {
      this.exception(new NotFound())
      done(false)
      return
    }

    done(true)
  }

  private async onRunJourney (id: number, done: (success: boolean) => void): Promise<void> {
    const affected = await this.journeyService.run(id)
    done(affected > 0)
  }

  private async onRunTimetable (id: number, done: (success: boolean) => void): Promise<void> {
    const timetable = await this.timetableService.get(id)

    if (timetable?.journey === undefined) {
      return
    }

    let affected = 0

    for (const journey of timetable.journey) {
      affected += await this.journeyService.run(journey.id)
    }

    done(affected > 0)
  }
}
