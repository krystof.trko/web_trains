import { Socket } from 'net'
import { events, HardwareTypes, lightState, OccupancySection, OledInfo, Packets, TrainFunction, TrainMotion, TrainMotionInstruction, UnitInfo, UnitInstruction } from 'web_trains_types'

import HardwareException from '../exception/HardwareException'

export default class HardwareTcp {
  constructor (private socket: Socket) {

  }

  /**
   * Send packet to hardware tcp server
   * @param event
   * @param data
   * @throws HardwareException
   */
  public sendPacket (event: events, data: Packets): void {
    if (this.socket.connecting || this.socket.destroyed) {
      let state = this.socket.connecting ? ' connecting' : ''
      state += this.socket.destroyed ? ' destroyed' : ''

      throw new HardwareException('Hardware is' + state, HardwareTypes.NotConnected)
    }

    const fnc = this.getEnumKey(events, event)
    const serialized = this[fnc](data as any)

    const packet = event + ':' + serialized.join(',') + '\n'
    const result = this.socket.write(packet)

    if (!result) {
      throw new HardwareException('Cannot send data to hardware', HardwareTypes.WriteError)
    }
  }

  public onData (listener: (data: Buffer) => void): void {
    this.socket.on('data', listener)
  }

  private unitInstruction (data: UnitInstruction) : (string|number)[] {
    const socket = [
      data.unitInstruction,
      data.numberOfUnit
    ]

    if (data.currentDelay) {
      socket.push(data.currentDelay)
    }

    return socket
  }

  private trainMotion (data: TrainMotion) : (string|number)[] {
    return [
      data.locomotive,
      data.speed,
      data.reverse
    ]
  }

  private trainMotionInstruction (data: TrainMotionInstruction) : (string|number)[] {
    return [
      ...this.trainMotion(data.trainMotion),
      data.section,
      data.waitTime
    ]
  }

  private trainFunction (data: TrainFunction) : (string|number)[] {
    return [
      data.locomotive,
      data.lights ? lightState.on : lightState.off
    ]
  }

  private unitInfo (data: UnitInfo) : (string|number)[] {
    return [
      data.unitInfo,
      data.numberOfUnit
    ]
  }

  private oledInfo (data: OledInfo) : (string|number)[] {
    return [
      data.messages.join('*')
    ]
  }

  private occupancySection (data: OccupancySection) : (string|number)[] {
    // TODO
    console.log(data)
    return ['']
  }

  private getEnumKey<T extends {[index:string]:string}> (myEnum:T, enumValue:string):keyof T {
    const keys = Object.keys(myEnum).filter(x => myEnum[x] === enumValue)
    return keys[0]
  }
}
