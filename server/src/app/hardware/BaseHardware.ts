import { Schema, Validator } from 'jsonschema'
import { Socket as IoSocket } from 'socket.io'
import { BadRequestTypes, events, Packets } from 'web_trains_types'

import Configuration from '../config/Configuration'
import Exception from '../exception/Exception'
import { BadRequest } from '../exception/http/BadRequest'
import socketHandler from '../exception/SocketHandler'
import HardwareTcp from './HardwareTcp'

export default abstract class BaseHardware {
  constructor (protected hw: HardwareTcp, protected io: IoSocket, protected config: Configuration) {
  }

  public abstract listen (): void

  /**
   * Packet to hardware
   * @param event
   * @param data
   * @returns boolean
   */
  protected sendPacket (event: events, data: Packets): boolean {
    try {
      this.hw.sendPacket(event, data)
    } catch (err) {
      this.exception(err)
      return false
    }

    return true
  }

  /**
   * Log exception and if enabled send message socket to client
   * @param err
   * @param sendToClient
   */
  protected exception (err: Error, sendToClient = true): Exception {
    const error = socketHandler(err, this.config.isProduction(), this.config.path(Configuration.PATH_LOG))

    if (sendToClient) {
      this.io.emit('exception', error)
    }

    return error
  }

  protected validate (data: Packets, validate: Schema): boolean {
    const result = new Validator().validate(data, validate)

    if (!result.valid) {
      this.exception(new BadRequest(result.toString(), BadRequestTypes.Validation))
    }

    return result.valid
  }
}
