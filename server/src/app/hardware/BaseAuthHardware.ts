import jsonWebToken, { TokenExpiredError } from 'jsonwebtoken'
import { Namespace, Socket as IoSocket } from 'socket.io'
import { Action, Resource, RolesManager, UnauthorizedTypes } from 'web_trains_types'

import Configuration from '../config/Configuration'
import { Unauthorized } from '../exception/http/Unathorized'
import { UserModel } from '../model/UserModel'
import UserService from '../service/UserService'
import BaseHardware from './BaseHardware'
import HardwareTcp from './HardwareTcp'

export default abstract class BaseAuthHardware extends BaseHardware {
  private userService = new UserService()
  private resource?: Resource
  private action?: Action

  constructor (hw: HardwareTcp, io: IoSocket, config: Configuration, ioNamespace: Namespace, private roles: RolesManager) {
    super(hw, io, config)
    ioNamespace.use(this.authMiddleware.bind(this))
  }

  /**
   * Set resource and action for authorization
   * must be called in listen()
   * @param resource
   * @param action
   */
  protected setResourceAction (resource: Resource, action: Action): void {
    this.resource = resource
    this.action = action
  }

  /**
   * Checks if user is authentificated and has permissions
   * @param socket
   * @param next
   * @return Promise<void>
   */
  private async authMiddleware (socket: IoSocket, next: (err?: Error|any) => void): Promise<void> {
    const jwt = socket.handshake.auth.token

    try {
      await this.auth(jwt)
      next()
    } catch (err) {
      const newErr = this.exception(err, false)

      // socket.io hack to transfer error data
      const socketError: any = new Error('socket error')
      socketError.data = newErr

      next(socketError)
    }
  }

  /**
   * Check if user can make action on resource
   * @param action
   * @param resource
   * @throws Unauthorized
   */
  private userCan (user: UserModel, action: Action, resource: Resource): UserModel {
    if (!user) {
      throw new Unauthorized('User is deleted', UnauthorizedTypes.DeletedUser)
    }

    if (!this.roles.can(user.role, action, resource)) {
      throw new Unauthorized('User ' + user.name + ' cannot ' + Action[action] + ' ' + Resource[resource])
    }

    return user
  }

  /**
   * JWT authorization
   * @param jwt
   * @throws Unauthorized
   */
  private async auth (jwt: string|undefined): Promise<void> {
    if (jwt === undefined) {
      throw new Unauthorized('No authorization header')
    }

    let userPayload: any

    try {
      userPayload = jsonWebToken.verify(jwt, this.config.app.secret)
    } catch (err) {
      if (err instanceof TokenExpiredError) {
        throw new Unauthorized(err.message, UnauthorizedTypes.TokenExpired)
      }

      throw new Unauthorized('JWT verify error')
    }

    if (userPayload.id === undefined) {
      throw new Unauthorized('Invalid payload')
    }

    if (this.action !== undefined && this.resource !== undefined) {
      const user = await this.userService.getById(userPayload.id)
      this.userCan(user, this.action, this.resource)
    }
  }
}
