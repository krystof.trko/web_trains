import { Action, events, Resource, UnitInstruction } from 'web_trains_types'

import BaseAuthHardware from './BaseAuthHardware'

export class AdminHardware extends BaseAuthHardware {
  public listen (): void {
    this.setResourceAction(Resource.hardwareAdmin, Action.crud)

    this.io.on(events.unitInstruction, this.onUnitInstruction.bind(this))
  }

  private onUnitInstruction (data: UnitInstruction, done: (success: boolean) => void) {
    this.validate(data, {
      properties: {
        unitInstruction: { type: 'string' },
        numberOfUnit: { type: 'number' },
        currentDelay: { type: 'number', minimum: 1, maximum: 255 }
      },
      required: ['unitInstruction', 'numberOfUnit']
    })

    const result = this.sendPacket(events.unitInstruction, data)

    done(result)
  }
}
