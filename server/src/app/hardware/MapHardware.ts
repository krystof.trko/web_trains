import { events, eventTransfer, OccupancySection } from 'web_trains_types'

import TrainService from '../service/TrainService'
import BaseHardware from './BaseHardware'

export class MapHardware extends BaseHardware {
  private trainService = new TrainService()

  public listen (): void {
    this.io.on(events.occupancySection, this.onOccupancySection.bind(this))
  }

  public async emit (): Promise<void> {
    const data = await this.trainService.getPositions()
    this.io.emit(eventTransfer.positions, data)
  }

  private onOccupancySection (data: OccupancySection): void {
    // TODO
    console.log(data)
  }
}
