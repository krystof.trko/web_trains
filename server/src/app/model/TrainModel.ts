import { Model, RelationMappings } from 'objection'

import { BaseModel } from './BaseModel'
import { StationModel } from './StationModel'
export class TrainModel extends BaseModel {
  static tableName = 'train'

  id!: number
  name!: string
  tcpName!: string
  image!: string|null
  light!: boolean
  speed!: number
  positionId!: number|null

  static jsonSchema = {
    type: 'object',
    properties: {
      id: { type: 'integer' },
      name: { type: 'string', minLength: 1, maxLength: 30 },
      tcpName: { type: 'string', minLength: 1, maxLength: 255 },
      image: { type: 'string', minLength: 1, maxLength: 255 },
      light: { type: 'boolean' },
      speed: { type: 'integer', minimum: -31, maximum: 31 },
      positionId: {
        oneOf: [
          { type: 'integer' },
          { type: 'null' }
        ]
      }
    },
    anyOf: [
      { required: ['name'] },
      { required: ['id'] },
      { required: ['tcpName'] },
      { required: ['image'] },
      { required: ['light'] },
      { required: ['speed'] },
      { required: ['positionId'] }
    ]
  }

  static get relationMappings (): RelationMappings {
    return {
      position: {
        relation: Model.BelongsToOneRelation,
        modelClass: StationModel,
        join: {
          from: 'train.position_id',
          to: 'station.id'
        }
      }
    }
  }
}
