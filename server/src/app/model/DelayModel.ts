import { Model, RelationMappings } from 'objection'

import { BaseModel } from './BaseModel'
import { StationModel } from './StationModel'
import { TrainModel } from './TrainModel'

export class DelayModel extends BaseModel {
  static tableName = 'delay'

  id!: number
  trainId!: number|null
  stationId!: number|null
  delay!: number

  static jsonSchema = {
    type: 'object',
    properties: {
      id: { type: 'integer' },
      trainId: {
        oneOf: [
          { type: 'integer' },
          { type: 'null' }
        ]
      },
      stationId: {
        oneOf: [
          { type: 'integer' },
          { type: 'null' }
        ]
      },
      delay: { type: 'integer', minimum: 0 }
    },
    anyOf: [
      { required: ['id'] },
      { required: ['trainId'] },
      { required: ['stationId'] },
      { required: ['delay'] }
    ]
  }

  static get relationMappings (): RelationMappings {
    return {
      train: {
        relation: Model.BelongsToOneRelation,
        modelClass: TrainModel,
        join: {
          from: 'delay.train_id',
          to: 'train.id'
        }
      },
      station: {
        relation: Model.BelongsToOneRelation,
        modelClass: StationModel,
        join: {
          from: 'delay.station_id',
          to: 'station.id'
        }
      }
    }
  }
}
