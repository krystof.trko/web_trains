import { Model, RelationMappings } from 'objection'

import { BaseModel } from './BaseModel'
import { JourneyModel } from './JourneyModel'

export class TimetableModel extends BaseModel {
  static tableName = 'timetable'

  id!: number
  name!: string
  sequence!: boolean

  static jsonSchema = {
    type: 'object',
    properties: {
      id: { type: 'integer' },
      name: { type: 'string', minLength: 1, maxLength: 255 },
      sequence: { type: 'boolean' },
      journey: {
        type: 'array',
        items: {
          type: 'integer'
        }
      }
    },
    anyOf: [
      { required: ['name'] },
      { required: ['journey'] }
    ]
  }

  static get relationMappings (): RelationMappings {
    return {
      journey: {
        relation: Model.ManyToManyRelation,
        modelClass: JourneyModel,
        join: {
          from: 'timetable.id',
          through: {
            from: 'timetable_journey.timetable_id',
            to: 'timetable_journey.journey_id'
          },
          to: 'journey.id'
        }
      }
    }
  }
}
