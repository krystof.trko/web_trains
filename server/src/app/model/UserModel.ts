import { Pojo } from 'objection'
import { Role } from 'web_trains_types'

import { BaseModel } from './BaseModel'
export class UserModel extends BaseModel {
  static tableName = 'user'

  id!: number
  name!: string
  role!: Role
  password!: string

  static jsonSchema = {
    type: 'object',
    properties: {
      id: { type: 'integer' },
      name: { type: 'string', minLength: 1, maxLength: 255 },
      role: {
        enum: [Role.admin, Role.editor, Role.user, Role.visitor]
      },
      password: { type: 'string', minLength: 6, maxLength: 255 }
    },
    oneOf: [
      { required: ['name', 'password'] },
      { required: ['id'] }
    ]
  }

  /**
   * Hide password hash in json
   * @param json
   * @returns Pojo
   */
  $formatJson (json: Pojo): Pojo {
    json = super.$formatJson(json)
    delete json.password
    return json
  }
}
