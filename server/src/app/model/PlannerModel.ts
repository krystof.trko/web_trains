import { Model, RelationMappings } from 'objection'
import { PlannerTime } from 'web_trains_types'

import { BaseModel } from './BaseModel'
import { TimetableModel } from './TimetableModel'

export class PlannerModel extends BaseModel {
  static tableName = 'planner'

  id!: number
  time!: PlannerTime[]|null
  day!: number[]|null
  month!: number[]|null
  once!: string|Date|null
  timetableId!: number|null

  static jsonSchema = {
    type: 'object',
    properties: {
      id: { type: 'integer' },
      time: {
        oneOf: [
          {
            type: 'array',
            items: {
              type: 'object',
              properties: {
                h: { type: 'integer', minimum: 0, maximum: 23 },
                m: { type: 'integer', minimum: 0, maximum: 59 }
              }
            }
          },
          { type: 'null' }
        ]
      },
      day: {
        oneOf: [
          {
            type: 'array',
            items: {
              type: 'integer',
              minimum: 0,
              maximum: 6
            }
          },
          { type: 'null' }
        ]
      },
      month: {
        oneOf: [
          {
            type: 'array',
            items: {
              type: 'integer',
              minimum: 0,
              maximum: 11
            }
          },
          { type: 'null' }
        ]
      },
      once: {
        oneOf: [
          { type: 'string' },
          { type: 'null' }
        ]
      }
    },
    anyOf: [
      { required: ['id'] },
      { required: ['time'] },
      { required: ['day'] },
      { required: ['month'] },
      { required: ['once'] },
      { required: ['timetableId'] }
    ]
  }

  static get relationMappings (): RelationMappings {
    return {
      timetable: {
        relation: Model.HasOneRelation,
        modelClass: TimetableModel,
        join: {
          from: 'planner.timetable_id',
          to: 'timetable.id'
        }
      }
    }
  }
}
