import { Model, RelationMappings } from 'objection'

import { dbDateTimeNow } from '../helper/dateTime'
import { BaseModel } from './BaseModel'
import { UserModel } from './UserModel'

export class CmsModel extends BaseModel {
  static tableName = 'cms'

  id!: number
  text!: string
  lastChanged!: string
  userId?: number|null

  static jsonSchema = {
    type: 'object',
    properties: {
      id: { type: 'integer' },
      text: { type: 'string' },
      userId: {
        oneOf: [
          { type: 'integer' },
          { type: 'null' }
        ]
      }
    },
    anyOf: [
      { required: ['text'] },
      { required: ['id'] }
    ]
  }

  $beforeInsert (): void {
    this.lastChanged = dbDateTimeNow()
  }

  $beforeUpdate (): void {
    this.lastChanged = dbDateTimeNow()
  }

  static get relationMappings (): RelationMappings {
    return {
      user: {
        relation: Model.BelongsToOneRelation,
        modelClass: UserModel,
        join: {
          from: 'cms.user_id',
          to: 'user.id'
        }
      }
    }
  }
}
