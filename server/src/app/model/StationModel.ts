import { BaseModel } from './BaseModel'

export class StationModel extends BaseModel {
  static tableName = 'station'

  id!: number
  name!: string
  tcpName!: string

  static jsonSchema = {
    type: 'object',
    properties: {
      id: { type: 'integer' },
      name: { type: 'string', minLength: 1, maxLength: 255 },
      tcpName: { type: 'string', minLength: 1, maxLength: 255 }
    },
    anyOf: [
      { required: ['name'] },
      { required: ['id'] },
      { required: ['tcpName'] }
    ]
  }
}
