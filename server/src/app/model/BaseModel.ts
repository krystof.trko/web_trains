import { ColumnNameMappers, Model, snakeCaseMappers } from 'objection'

export class BaseModel extends Model {
  static get columnNameMappers (): ColumnNameMappers {
    return snakeCaseMappers()
  }
}
