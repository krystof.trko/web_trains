import { Model, RelationMappings } from 'objection'
import { JourneyDirection } from 'web_trains_types'

import { BaseModel } from './BaseModel'
import { StationModel } from './StationModel'
import { TrainModel } from './TrainModel'

export class JourneyModel extends BaseModel {
  static tableName = 'journey'

  id!: number
  stationFromId!: number|null
  stationToId!: number|null
  trainId!: number|null
  direction!: JourneyDirection
  speed!: number
  running!: boolean

  static jsonSchema = {
    type: 'object',
    properties: {
      id: { type: 'integer' },
      stationFromId: {
        oneOf: [
          { type: 'integer' },
          { type: 'null' }
        ]
      },
      stationToId: {
        oneOf: [
          { type: 'integer' },
          { type: 'null' }
        ]
      },
      trainId: {
        oneOf: [
          { type: 'integer' },
          { type: 'null' }
        ]
      },
      speed: { type: 'integer', minimum: 0, maximum: 31 },
      running: { type: 'boolean' },
      direction: {
        enum: [JourneyDirection.ahead, JourneyDirection.reverse]
      },
      section: {
        type: 'array',
        items: {
          type: 'integer'
        }
      }
    },
    anyOf: [
      { required: ['stationFromId'] },
      { required: ['stationToId'] },
      { required: ['trainId'] },
      { required: ['direction'] },
      { required: ['speed'] },
      { required: ['section'] }
    ]
  }

  static get relationMappings (): RelationMappings {
    return {
      stationFrom: {
        relation: Model.BelongsToOneRelation,
        modelClass: StationModel,
        join: {
          from: 'journey.station_from_id',
          to: 'station.id'
        }
      },
      stationTo: {
        relation: Model.BelongsToOneRelation,
        modelClass: StationModel,
        join: {
          from: 'journey.station_to_id',
          to: 'station.id'
        }
      },
      train: {
        relation: Model.BelongsToOneRelation,
        modelClass: TrainModel,
        join: {
          from: 'journey.train_id',
          to: 'train.id'
        }
      },
      section: {
        relation: Model.ManyToManyRelation,
        modelClass: StationModel,
        join: {
          from: 'journey.id',
          through: {
            from: 'journey_station.journey_id',
            to: 'journey_station.station_id'
          },
          to: 'station.id'
        }
      }
    }
  }
}
