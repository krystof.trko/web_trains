import Koa from 'koa'
import serve from 'koa-static'
import historyApiFallback from 'koa2-connect-history-api-fallback'

import Configuration from './config/Configuration'

export default class ClientBootstrap {
  private app: Koa

  constructor (private config: Configuration) {
    this.app = new Koa()
  }

  /**
   * Run client application
   * @returns Koa
   */
  public run (): Koa {
    // One page app redirect
    this.app.use(historyApiFallback({
      index: 'index.html',
      whiteList: [Configuration.ADRESS_SOCKET, Configuration.ADRESS_API]
    }))

    this.app.use(serve(this.config.path(Configuration.PATH_CLIENT)))

    return this.app
  }
}
