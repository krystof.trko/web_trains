import { Server as HttpServer } from 'http'
import { Server } from 'socket.io'
import { Namespace, RolesManager } from 'web_trains_types'

import Configuration from './config/Configuration'
import { AdminHardware } from './hardware/AdminHardware'
import { AdminMapHardware } from './hardware/AdminMapHardware'
import HardwareTcp from './hardware/HardwareTcp'
import { MapHardware } from './hardware/MapHardware'

export default class ApiBootstrap {
  private ioServer: Server
  private roles: RolesManager

  constructor (private config: Configuration, private hwSocket: HardwareTcp, httpServer: HttpServer) {
    this.roles = new RolesManager()

    this.ioServer = new Server(httpServer, {
      path: Configuration.ADRESS_SOCKET,
      serveClient: false,
      cors: {
        origin: '*',
        methods: ['GET', 'POST'],
        allowedHeaders: ['Authorization'],
        credentials: true
      }
    })
  }

  /**
   * Run socket aplication
   * @returns Server
   */
  public run (): Server {
    const adminHardware = this.ioServer.of('/' + Namespace.adminHardware)
    const adminMap = this.ioServer.of('/' + Namespace.adminMap)
    const publicMap = this.ioServer.of('/' + Namespace.publicMap)

    adminHardware.on('connection', (ioSocket) => { new AdminHardware(this.hwSocket, ioSocket, this.config, adminHardware, this.roles).listen() })
    adminMap.on('connection', (ioSocket) => { new AdminMapHardware(this.hwSocket, ioSocket, this.config, adminMap, this.roles).listen() })
    publicMap.on('connection', (ioSocket) => {
      const mapHardware = new MapHardware(this.hwSocket, ioSocket, this.config)
      mapHardware.listen()

      setInterval(async () => {
        await mapHardware.emit()
      }, 5000) // 5sec

      mapHardware.emit()
    })

    return this.ioServer
  }
}
