import { ExceptionBody } from 'web_trains_types'

export default class Exception extends Error {
  constructor (public body: Partial<ExceptionBody>, public status: number) {
    super(body.message)
  }
}
