import { createLogger, format, Logger, transports } from 'winston'

/**
 * In production log to file in development to console
 * @param isProduction
 * @param logPath
 * @param err
 * @param level
 */
export function log (isProduction: boolean, logPath: string, err: unknown, level: string): void {
  const logger = isProduction ? productionLogger(logPath) : developmentLogger()
  logger.log(level, err)
}

class FileOneLevelTransport extends transports.File {
  log (info: any, callback: () => void) {
    if (super.log === undefined || info.level !== this.level) {
      return
    }

    super.log(info, callback)
  }
}

/**
 * Create winston production logger
 * @param logPath
 * @returns
 */
function productionLogger (logPath: string): Logger {
  return createLogger({
    format: format.combine(
      format.timestamp({
        format: 'YYYY-MM-DD HH:mm:ss'
      }),
      format.errors({ stack: true }),
      format.json()
    ),
    transports: [
      new FileOneLevelTransport({ filename: logPath + 'error.log', level: 'error' }),
      new FileOneLevelTransport({ filename: logPath + 'info.log', level: 'info' }),
      new FileOneLevelTransport({ filename: logPath + 'client.log', level: 'http' }),
      new FileOneLevelTransport({ filename: logPath + 'hardware.log', level: 'warn' })
    ],
    defaultMeta: { service: 'WebTrains' }
  })
}

/**
 * Create winston development logger
 */
function developmentLogger (): Logger {
  return createLogger({
    level: 'silly',
    format: format.combine(
      format.errors({ stack: true }),
      format.timestamp(),
      format.prettyPrint(),
      format.colorize()
    ),
    transports: [
      new transports.Console()
    ]
  })
}

/**
 * Log level by error code
 * @param status
 * @returns string
 */
export function getLevel (status: number): string {
  const errorType = Math.floor(status / 100)
  return errorType === 4 || errorType === 3 ? 'info' : 'error'
}
