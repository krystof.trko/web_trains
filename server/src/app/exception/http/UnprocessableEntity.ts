import { HttpTypes } from 'web_trains_types'

import Exception from '../Exception'

/**
 * The server understands the content type of the request entity, but was unable to process the contained instructions.
 */
export class UnprocessableEntity extends Exception {
  constructor (message = 'Unprocessable Entity') {
    const body = {
      message: message
    }

    super(body, HttpTypes.UnprocessableEntity)
  }
}
