import { HttpTypes, UnauthorizedTypes } from 'web_trains_types'

import Exception from '../Exception'

/**
 * Error code response for missing or invalid authentication token.
 */
export class Unauthorized extends Exception {
  constructor (message = 'Unauthorized', type: UnauthorizedTypes | null = null) {
    const body = {
      message: message,
      type: type
    }

    super(body, HttpTypes.Unauthorized)
  }
}
