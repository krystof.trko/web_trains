import { BadRequestTypes, HttpTypes } from 'web_trains_types'

import Exception from '../Exception'

/**
 * General error for when fulfilling the request would cause an invalid state.
 * Domain validation errors, missing data, etc. are some examples.
 */
export class BadRequest extends Exception {
  constructor (message = 'Bad Request', type: BadRequestTypes | null = null) {
    const body = {
      message: message,
      type: type
    }

    super(body, HttpTypes.BadRequest)
  }
}
