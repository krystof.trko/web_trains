import { HttpTypes } from 'web_trains_types'

import Exception from '../Exception'
/**
 * Error code for when the user is not authorized to perform the operation
 * or the resource is unavailable for some reason (e.g. time constraints, etc.).
 */
export class Forbidden extends Exception {
  constructor (message = 'Forbidden') {
    const body = {
      message: message
    }

    super(body, HttpTypes.Forbidden)
  }
}
