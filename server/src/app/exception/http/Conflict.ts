import { ConflictTypes, HttpTypes } from 'web_trains_types'

import Exception from '../Exception'

/**
 * Whenever a resource conflict would be caused by fulfilling the request.
 * Duplicate entries, such as trying to create two customers with the same information
 * and deleting root objects when cascade-delete is not supported are a couple of examples.
 */
export class Conflict extends Exception {
  constructor (message = 'Conflict', type: ConflictTypes | null = null) {
    const body = {
      message: message,
      type: type
    }

    super(body, HttpTypes.Conflict)
  }
}
