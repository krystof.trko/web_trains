import { HttpTypes, NotFoundTypes } from 'web_trains_types'

import Exception from '../Exception'

/**
 * Used when the requested resource is not found,
 * whether it doesn't exist or if there was a 401 or 403 that,
 * for security reasons, the service wants to mask.
 */
export class NotFound extends Exception {
  constructor (message = 'Not Found', type: NotFoundTypes | null = null) {
    const body = {
      message: message,
      type: type
    }

    super(body, HttpTypes.NotFound)
  }
}
