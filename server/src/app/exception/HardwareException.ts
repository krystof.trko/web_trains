import { HardwareTypes } from 'web_trains_types'

import Exception from './Exception'

export default class HardwareException extends Exception {
  constructor (message = 'Hardware error', type: HardwareTypes | null = null) {
    const body = {
      message: message,
      type: type
    }

    super(body, 500)
  }
}
