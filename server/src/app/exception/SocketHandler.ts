import Exception from './Exception'
import { getLevel, log } from './Logger'

export default function (err: Error, isProduction: boolean, logPath: string): Exception {
  const newErr = getNewError(err)
  log(isProduction, logPath, err, getLevel(newErr.status))

  return newErr
}

function getNewError (err: any): Exception {
  if (err instanceof Exception) {
    return err
  }

  return new Exception({
    message: 'Internal Server Error'
  }, 500)
}
