import { Next, ParameterizedContext } from 'koa'
import {
  CheckViolationError, ConstraintViolationError, DataError, DBError, ForeignKeyViolationError, NotFoundError, NotNullViolationError, UniqueViolationError, ValidationError
} from 'objection'
import { BadRequestTypes, ConflictTypes, NotFoundTypes } from 'web_trains_types'

import ClientException from './ClientException'
import Exception from './Exception'
import { BadRequest } from './http/BadRequest'
import { Conflict } from './http/Conflict'
import { NotFound } from './http/NotFound'
import { getLevel, log } from './Logger'

export default (isProduction: boolean, logPath: string) => {
  return async (ctx: ParameterizedContext, next: Next): Promise<void> => {
    return next()
      .then(() => {
        if (!ctx || (ctx.status === 404 && ctx.body == null)) {
          ctx.throw(new NotFound())
        }
      })
      .catch(err => {
        if (err instanceof ClientException) {
          log(isProduction, logPath, err.message, 'http')
          ctx.type = 'json'
          ctx.body = {}
          ctx.status = 200
          return
        }

        const newErr = getNewError(err)

        ctx.type = 'json'
        ctx.body = newErr.body
        ctx.status = newErr.status

        const level = getLevel(newErr.status)

        log(isProduction, logPath, err, level)
      })
  }
}

function getNewError (err: any): Exception {
  if (err instanceof Exception) {
    return err
  }

  if (err instanceof NotFoundError) {
    return new NotFound('Database error', NotFoundTypes.DbNotFound)
  }

  if (err instanceof UniqueViolationError) {
    return new Conflict('Database error', ConflictTypes.DbUniqueViolation)
  }

  if (err instanceof ForeignKeyViolationError) {
    return new Conflict('Database error', ConflictTypes.DbForeignKeyViolation)
  }

  if (err instanceof ValidationError) {
    return new BadRequest(err.message, BadRequestTypes.Validation)
  }

  if (err instanceof NotNullViolationError) {
    return new BadRequest('Database error', BadRequestTypes.DbNotNullViolation)
  }

  if (err instanceof CheckViolationError) {
    return new BadRequest('Database error', BadRequestTypes.DbCheckViolation)
  }

  if (err instanceof DataError) {
    return new BadRequest('Database error', BadRequestTypes.DbData)
  }

  if (err instanceof ConstraintViolationError) {
    return new BadRequest('Database error', BadRequestTypes.DbConstraintViolationError)
  }

  if (err instanceof SyntaxError) {
    return new BadRequest(err.message)
  }

  if (err instanceof DBError) {
    return new Exception({
      message: 'Database error'
    }, 500)
  }

  return new Exception({
    message: 'Internal Server Error'
  }, 500)
}
