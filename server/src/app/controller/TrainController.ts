
import { Next, ParameterizedContext } from 'koa'
import { ApiUrl } from 'web_trains_types'

import { NotFound } from '../exception/http/NotFound'
import TrainService from '../service/TrainService'
import BaseAuthController from './BaseAuthController'

/**
 * Routes for train
 */
export default class TrainController extends BaseAuthController {
  private trainService = new TrainService()

  public setRouter (): void {
    this.router.get(ApiUrl.train, this.getAll.bind(this))

    this.router.patch(ApiUrl.train + '/:id', this.auth.bind(this), this.patch.bind(this))
  }

  /**
   * Get all trains
   * @param ctx
   * @param next
   * @throws NotFound
   */
  private async getAll (ctx: ParameterizedContext, next: Next): Promise<void> {
    const onlyWithPos = !!ctx.request.query.onlyWithPos

    ctx.body = await this.trainService.getAll(onlyWithPos)

    if (ctx.body === []) {
      throw new NotFound()
    }

    await next()
  }

  /**
   * Patch train position
   * @param ctx
   * @param next
   * @throws NotFound
   */
  private async patch (ctx: ParameterizedContext, next: Next): Promise<void> {
    const body = this.bodyId(ctx)
    const res = await this.trainService.setPosition(body.id, body.positionId)

    if (res === 0) {
      throw new NotFound()
    }

    ctx.body = {}

    await next()
  }
}
