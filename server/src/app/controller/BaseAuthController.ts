import jsonWebToken, { TokenExpiredError } from 'jsonwebtoken'
import { Next, ParameterizedContext } from 'koa'
import { Action, Resource, UnauthorizedTypes } from 'web_trains_types'

import { Unauthorized } from '../exception/http/Unathorized'
import { UserModel } from '../model/UserModel'
import UserService from '../service/UserService'
import BaseController from './BaseController'

export default abstract class BaseAuthController extends BaseController {
  protected user?: UserModel

  protected userService = new UserService()

  /**
   * Check if user can make action on resource
   * @param action
   * @param resource
   * @throws Unauthorized
   */
  public userCan (action: Action, resource: Resource): UserModel {
    if (!this.user) {
      throw new Unauthorized('User is deleted', UnauthorizedTypes.DeletedUser)
    }

    if (!this.roles.can(this.user.role, action, resource)) {
      throw new Unauthorized('User ' + this.user.name + ' cannot ' + Action[action] + ' ' + Resource[resource])
    }

    return this.user
  }

  /**
   * JWT authorization middleware
   * @param ctx
   * @param next
   * @throws Unauthorized
   */
  protected async auth (ctx: ParameterizedContext, next: Next): Promise<void> {
    const jwt = this.getJwt(ctx)

    let userPayload: any

    try {
      userPayload = jsonWebToken.verify(jwt, this.config.app.secret)
    } catch (err) {
      if (err instanceof TokenExpiredError) {
        throw new Unauthorized(err.message, UnauthorizedTypes.TokenExpired)
      }

      throw new Unauthorized('JWT verify error')
    }

    if (userPayload.id === undefined) {
      throw new Unauthorized('Invalid payload')
    }

    this.user = await this.userService.getById(userPayload.id)

    await next()
  }

  /**
   * Get JWT from headers
   * @param ctx
   * @throws Unauthorized
   * @returns string
   */
  private getJwt (ctx: ParameterizedContext): string {
    if (ctx.request.header.authorization === undefined) {
      throw new Unauthorized('No authorization header')
    }

    const jwtParts = ctx.request.header.authorization.split(' ')

    if (jwtParts.length !== 2 && jwtParts[0] !== 'Bearer') {
      throw new Unauthorized('Authorization header must be Bearer')
    }

    return jwtParts[1]
  }
}
