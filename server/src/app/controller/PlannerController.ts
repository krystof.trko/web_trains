
import { Next, ParameterizedContext } from 'koa'
import { Action, ApiUrl, PlannerAdd, Resource } from 'web_trains_types'

import { NotFound } from '../exception/http/NotFound'
import { PlannerModel } from '../model/PlannerModel'
import PlannerService from '../service/PlannerService'
import BaseAuthController from './BaseAuthController'

/**
 * Routes for planner
 */
export default class PlannerController extends BaseAuthController {
  private plannerService = new PlannerService()

  public setRouter (): void {
    this.router.get(ApiUrl.planner, this.getAll.bind(this))
    this.router.get(ApiUrl.planner + '/range/:from/:to', this.getRange.bind(this))
    this.router.get(ApiUrl.planner + '/:id', this.get.bind(this))

    this.router.post(ApiUrl.planner, this.auth.bind(this), this.add.bind(this))
    this.router.patch(ApiUrl.planner + '/:id', this.auth.bind(this), this.patch.bind(this))
    this.router.del(ApiUrl.planner + '/:id', this.auth.bind(this), this.del.bind(this))
  }

  /**
   * Get planner by id
   * @param ctx
   * @param next
   * @throws NotFound
   */
  private async get (ctx: ParameterizedContext, next: Next): Promise<void> {
    const id = this.paramId(ctx)
    ctx.body = await this.plannerService.get(id)

    if (ctx.body === undefined) {
      throw new NotFound()
    }

    await next()
  }

  /**
   * Get comming planners in range from to
   * @param ctx
   * @param next
   * @throws NotFound
   */
  private async getRange (ctx: ParameterizedContext, next: Next): Promise<void> {
    const from = ctx.params.from
    const to = ctx.params.to

    ctx.body = await this.plannerService.getRange(from, to)

    if (ctx.body === undefined) {
      throw new NotFound()
    }

    await next()
  }

  /**
   * Get all planners
   * @param ctx
   * @param next
   * @throws NotFound
   */
  private async getAll (ctx: ParameterizedContext, next: Next): Promise<void> {
    ctx.body = await this.plannerService.getAll()

    if (ctx.body === []) {
      throw new NotFound()
    }

    await next()
  }

  /**
   * Add planner
   * @param ctx
   * @param next
   */
  private async add (ctx: ParameterizedContext, next: Next): Promise<void> {
    this.userCan(Action.crud, Resource.timetable)
    const data = <PlannerAdd>PlannerModel.fromJson(ctx.request.body)

    ctx.body = this.plannerService.add(data)

    await next()
  }

  /**
   * Delete planner by id
   * @param ctx
   * @param next
   * @throws NotFound
   */
  private async del (ctx: ParameterizedContext, next: Next): Promise<void> {
    this.userCan(Action.crud, Resource.timetable)
    const id = this.paramId(ctx)

    const numb = await this.plannerService.delete(id)

    if (numb <= 0) {
      throw new NotFound()
    }

    ctx.body = {}

    await next()
  }

  /**
   * Update planner with id
   * @param ctx
   * @param next
   * @throws NotFound
   */
  private async patch (ctx: ParameterizedContext, next: Next): Promise<void> {
    this.userCan(Action.crud, Resource.timetable)
    const data = PlannerModel.fromJson(ctx.request.body)
    const id = this.paramId(ctx)

    const numb = await this.plannerService.patch(id, data)

    if (numb <= 0) {
      throw new NotFound()
    }

    ctx.body = {}

    await next()
  }
}
