
import { Next, ParameterizedContext } from 'koa'
import { Action, ApiUrl, Resource, TimetableAdd } from 'web_trains_types'

import { BadRequest } from '../exception/http/BadRequest'
import { NotFound } from '../exception/http/NotFound'
import { TimetableModel } from '../model/TimetableModel'
import TimetableService from '../service/TimetableService'
import BaseAuthController from './BaseAuthController'

/**
 * Routes for timetable
 */
export default class TimetableController extends BaseAuthController {
  private timetableService = new TimetableService()

  public setRouter (): void {
    this.router.get(ApiUrl.timetable, this.getAll.bind(this))
    this.router.get(ApiUrl.timetable + '/:id', this.get.bind(this))

    this.router.post(ApiUrl.timetable, this.auth.bind(this), this.add.bind(this))
    this.router.patch(ApiUrl.timetable + '/:id', this.auth.bind(this), this.patch.bind(this))
    this.router.del(ApiUrl.timetable + '/:id', this.auth.bind(this), this.del.bind(this))
  }

  /**
   * Get timetable by id
   * @param ctx
   * @param next
   * @throws NotFound
   */
  private async get (ctx: ParameterizedContext, next: Next): Promise<void> {
    const id = this.paramId(ctx)
    ctx.body = await this.timetableService.get(id)

    if (ctx.body === undefined) {
      throw new NotFound()
    }

    await next()
  }

  /**
   * Get all timetables
   * @param ctx
   * @param next
   * @throws NotFound
   */
  private async getAll (ctx: ParameterizedContext, next: Next): Promise<void> {
    ctx.body = await this.timetableService.getAll()

    if (ctx.body === []) {
      throw new NotFound()
    }

    await next()
  }

  /**
   * Add timetable
   * @param ctx
   * @param next
   */
  private async add (ctx: ParameterizedContext, next: Next): Promise<void> {
    this.userCan(Action.crud, Resource.timetable)
    const data = <TimetableAdd><unknown>TimetableModel.fromJson(ctx.request.body)

    if (data.journey === undefined) {
      throw new BadRequest('journey is missing')
    }

    ctx.body = await this.timetableService.add(data)

    await next()
  }

  /**
   * Delete timetable by id
   * @param ctx
   * @param next
   * @throws NotFound
   */
  private async del (ctx: ParameterizedContext, next: Next): Promise<void> {
    this.userCan(Action.crud, Resource.timetable)
    const id = this.paramId(ctx)

    const numb = await this.timetableService.delete(id)

    if (numb <= 0) {
      throw new NotFound()
    }

    ctx.body = {}

    await next()
  }

  /**
   * Update timetable with id
   * @param ctx
   * @param next
   * @throws NotFound
   */
  private async patch (ctx: ParameterizedContext, next: Next): Promise<void> {
    this.userCan(Action.crud, Resource.timetable)
    const data = TimetableModel.fromJson(ctx.request.body)
    const id = this.paramId(ctx)

    const numb = await this.timetableService.patch(id, data)

    if (numb <= 0) {
      throw new NotFound()
    }

    ctx.body = {}

    await next()
  }
}
