
import { Next, ParameterizedContext } from 'koa'

import BaseController from './BaseController'

/**
 * Public routes for testing api
 */
export default class TestController extends BaseController {
  public setRouter (): void {
    this.router.get('/test', this.test)
    this.router.post('/test', this.test)
    this.router.patch('/test', this.test)
    this.router.put('/test', this.test)
    this.router.del('/test', this.test)
  }

  /**
   * Return message API works
   * @param ctx
   * @param next
   */
  private async test (ctx: ParameterizedContext, next: Next): Promise<void> {
    ctx.body = {
      message: 'API works'
    }

    await next()
  }
}
