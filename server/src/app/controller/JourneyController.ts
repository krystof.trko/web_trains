
import { Next, ParameterizedContext } from 'koa'
import { Action, ApiUrl, JourneyAdd, Resource } from 'web_trains_types'

import { NotFound } from '../exception/http/NotFound'
import { JourneyModel } from '../model/JourneyModel'
import JourneyService from '../service/JourneyService'
import BaseAuthController from './BaseAuthController'

/**
 * Routes for journey
 */
export default class JourneyController extends BaseAuthController {
  private journeyService = new JourneyService()

  public setRouter (): void {
    this.router.get(ApiUrl.journey, this.getAll.bind(this))
    this.router.get(ApiUrl.journey + '/:id', this.get.bind(this))

    this.router.post(ApiUrl.journey, this.auth.bind(this), this.add.bind(this))
    this.router.patch(ApiUrl.journey + '/:id', this.auth.bind(this), this.patch.bind(this))
    this.router.del(ApiUrl.journey + '/:id', this.auth.bind(this), this.del.bind(this))
  }

  /**
   * Get journey by id
   * @param ctx
   * @param next
   * @throws NotFound
   */
  private async get (ctx: ParameterizedContext, next: Next): Promise<void> {
    const id = this.paramId(ctx)
    ctx.body = await this.journeyService.get(id)

    if (ctx.body === undefined) {
      throw new NotFound()
    }

    await next()
  }

  /**
   * Get all journeys
   * @param ctx
   * @param next
   * @throws NotFound
   */
  private async getAll (ctx: ParameterizedContext, next: Next): Promise<void> {
    ctx.body = await this.journeyService.getAll()

    if (ctx.body === []) {
      throw new NotFound()
    }

    await next()
  }

  /**
   * Add journey
   * @param ctx
   * @param next
   */
  private async add (ctx: ParameterizedContext, next: Next): Promise<void> {
    this.userCan(Action.crud, Resource.timetable)
    const data = <JourneyAdd><unknown>JourneyModel.fromJson(ctx.request.body)

    ctx.body = await this.journeyService.add(data)

    await next()
  }

  /**
   * Delete journey by id
   * @param ctx
   * @param next
   * @throws NotFound
   */
  private async del (ctx: ParameterizedContext, next: Next): Promise<void> {
    this.userCan(Action.crud, Resource.timetable)
    const id = this.paramId(ctx)

    const numb = await this.journeyService.delete(id)

    if (numb <= 0) {
      throw new NotFound()
    }

    ctx.body = {}

    await next()
  }

  /**
   * Update journey with id
   * @param ctx
   * @param next
   * @throws NotFound
   */
  private async patch (ctx: ParameterizedContext, next: Next): Promise<void> {
    this.userCan(Action.crud, Resource.timetable)
    const data = JourneyModel.fromJson(ctx.request.body)
    const id = this.paramId(ctx)

    const numb = await this.journeyService.patch(id, data)

    if (numb <= 0) {
      throw new NotFound()
    }

    ctx.body = {}

    await next()
  }
}
