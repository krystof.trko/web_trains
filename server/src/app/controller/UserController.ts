import { Next, ParameterizedContext } from 'koa'
import { Action, ApiUrl, Resource, UnauthorizedTypes } from 'web_trains_types'

import { NotFound } from '../exception/http/NotFound'
import { Unauthorized } from '../exception/http/Unathorized'
import { UserModel } from '../model/UserModel'
import BaseAuthController from './BaseAuthController'

/**
 * User editing for looged user and admin
 */
export default class UserController extends BaseAuthController {
  public setRouter (): void {
    // My account
    this.router.get(ApiUrl.me, this.auth.bind(this), this.getLoggedUser.bind(this))
    this.router.patch(ApiUrl.me, this.auth.bind(this), this.patchLoggedUser.bind(this))
    this.router.del(ApiUrl.me, this.auth.bind(this), this.delLoggedUser.bind(this))

    // Admin user editor
    this.router.get(ApiUrl.user, this.auth.bind(this), this.getAllUser.bind(this))
    this.router.patch(ApiUrl.user + '/:id', this.auth.bind(this), this.patchUser.bind(this))
    this.router.del(ApiUrl.user + '/:id', this.auth.bind(this), this.delUser.bind(this))
  }

  /**
   * Current user data
   * @param ctx
   * @param next
   * @throws Unauthorized
   * @throws NotFound
   */
  private async getLoggedUser (ctx: ParameterizedContext, next: Next): Promise<void> {
    if (this.user === undefined) {
      throw new Unauthorized('User is deleted', UnauthorizedTypes.DeletedUser)
    }

    ctx.body = this.user

    await next()
  }

  /**
   * Change current user data
   * User id os current user
   * Cannot change role without permission update Resource.userAdmin
   * @param ctx
   * @param next
   * @throws Unauthorized
   * @throws NotFound
   */
  private async patchLoggedUser (ctx: ParameterizedContext, next: Next): Promise<void> {
    if (this.user === undefined) {
      throw new Unauthorized('User is deleted', UnauthorizedTypes.DeletedUser)
    }

    const body = ctx.request.body

    body.id = this.user.id

    if (body.role !== undefined) {
      this.userCan(Action.crud, Resource.userAdmin)
    }

    const userData = UserModel.fromJson(body)

    const numb = await this.userService.patch(userData)

    if (numb <= 0) {
      throw new NotFound()
    }

    ctx.body = {}
    await next()
  }

  /**
    * Delete current user
    * @param ctx
    * @param next
    * @throws Unauthorized
    * @throws NotFound
    */
  private async delLoggedUser (ctx: ParameterizedContext, next: Next): Promise<void> {
    if (this.user === undefined) {
      throw new Unauthorized('User is deleted', UnauthorizedTypes.DeletedUser)
    }

    const numb = await this.userService.delete(this.user.id)

    if (numb <= 0) {
      throw new NotFound()
    }

    ctx.body = {}
    await next()
  }

  /**
    * All user for administration
    * @param ctx
    * @param next
    */
  private async getAllUser (ctx: ParameterizedContext, next: Next): Promise<void> {
    this.userCan(Action.crud, Resource.userAdmin)

    ctx.body = await this.userService.getAll()

    await next()
  }

  /**
    * Change user by administrator
    * @param ctx
    * @param next
    * @throws NotFound
    */
  private async patchUser (ctx: ParameterizedContext, next: Next): Promise<void> {
    this.userCan(Action.crud, Resource.userAdmin)

    const body = this.bodyId(ctx)

    const userData = UserModel.fromJson(body)

    const numb = await this.userService.patch(userData)

    if (numb <= 0) {
      throw new NotFound()
    }

    ctx.body = {}
    await next()
  }

  /**
    * Delete user by administrator
    * @param ctx
    * @param next
    * @throws NotFound
    */
  private async delUser (ctx: ParameterizedContext, next: Next): Promise<void> {
    this.userCan(Action.crud, Resource.userAdmin)

    const id = this.paramId(ctx)

    const numb = await this.userService.delete(id)

    if (numb <= 0) {
      throw new NotFound()
    }

    ctx.body = {}
    await next()
  }
}
