
import { Next, ParameterizedContext } from 'koa'
import { Action, ApiUrl, Resource } from 'web_trains_types'

import { NotFound } from '../exception/http/NotFound'
import { DelayModel } from '../model/DelayModel'
import DelayService from '../service/DelayService'
import StationService from '../service/StationService'
import TrainService from '../service/TrainService'
import BaseAuthController from './BaseAuthController'

/**
 * Routes for delay admin
 */
export default class DelayController extends BaseAuthController {
  private delayService = new DelayService()
  private trainService = new TrainService()
  private stationService = new StationService()

  public setRouter (): void {
    this.router.get(ApiUrl.delay, this.auth.bind(this), this.getAll.bind(this))
    this.router.put(ApiUrl.delay, this.auth.bind(this), this.put.bind(this))
  }

  /**
   * Get all delays, trains and stations
   * @param ctx
   * @param next
   * @throws NotFound
   */
  private async getAll (ctx: ParameterizedContext, next: Next): Promise<void> {
    this.userCan(Action.see, Resource.userAdmin)

    const data = {
      delay: await this.delayService.getAll(),
      train: await this.trainService.getAll(false),
      station: await this.stationService.getAll()
    }

    if (!data.delay || !data.train || !data.station) {
      throw new NotFound()
    }

    ctx.body = data

    await next()
  }

  /**
   * Get all stations
   * @param ctx
   * @param next
   * @throws NotFound
   */
  private async put (ctx: ParameterizedContext, next: Next): Promise<void> {
    this.userCan(Action.see, Resource.userAdmin)

    const data = DelayModel.fromJson(ctx.request.body)

    ctx.body = await this.delayService.put(data)

    if (ctx.body === 0) {
      throw new NotFound()
    }

    await next()
  }
}
