
import { Next, ParameterizedContext } from 'koa'
import { ApiUrl } from 'web_trains_types'

import { NotFound } from '../exception/http/NotFound'
import StationService from '../service/StationService'
import BaseController from './BaseController'

/**
 * Routes for station
 */
export default class StationController extends BaseController {
  private stationService = new StationService()

  public setRouter (): void {
    this.router.get(ApiUrl.station, this.getAll.bind(this))
  }

  /**
   * Get all stations
   * @param ctx
   * @param next
   * @throws NotFound
   */
  private async getAll (ctx: ParameterizedContext, next: Next): Promise<void> {
    ctx.body = await this.stationService.getAll()

    if (ctx.body === []) {
      throw new NotFound()
    }

    await next()
  }
}
