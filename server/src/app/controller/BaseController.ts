import { ParameterizedContext } from 'koa'
import KoaRouter from 'koa-router'
import { RolesManager } from 'web_trains_types'

import Configuration from '../config/Configuration'
import { NotFound } from '../exception/http/NotFound'

export default abstract class BaseController {
  constructor (protected router: KoaRouter, protected roles: RolesManager, protected config: Configuration) {

  }

  /**
   * Add routes
   */
  public abstract setRouter(): void

  /**
   * Get body with id from param for patch
   * @param ctx
   * @returns any
   */
  protected bodyId (ctx: ParameterizedContext): any {
    const body = ctx.request.body
    body.id = this.paramId(ctx)

    return body
  }

  /**
   * Get number from context parameter
   * @param ctx
   * @throws NotFound
   * @returns number
   */
  protected paramId (ctx: ParameterizedContext): number {
    const id = Number(ctx.params.id)

    if (isNaN(id)) {
      throw new NotFound()
    }

    return id
  }
}
