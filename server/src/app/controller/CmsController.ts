
import { Next, ParameterizedContext } from 'koa'
import { Action, ApiUrl, Resource } from 'web_trains_types'

import { NotFound } from '../exception/http/NotFound'
import { CmsModel } from '../model/CmsModel'
import CmsService from '../service/CmsService'
import BaseAuthController from './BaseAuthController'

/**
 * Routes for changing and getting CMS page
 */
export default class CmsController extends BaseAuthController {
  private cmsService = new CmsService()

  public setRouter (): void {
    this.router.get(ApiUrl.cms + '/:id', this.get.bind(this))
    this.router.patch(ApiUrl.cms + '/:id', this.auth.bind(this), this.patch.bind(this))
  }

  /**
   * Get CMS page by id - public route
   * @param ctx
   * @param next
   * @throws NotFound
   */
  private async get (ctx: ParameterizedContext, next: Next): Promise<void> {
    const id = this.paramId(ctx)
    ctx.body = await this.cmsService.get(id)

    if (ctx.body === undefined) {
      throw new NotFound()
    }

    await next()
  }

  /**
   * Change CMS page by id
   * @param ctx
   * @param next
   * @throws NotFound
   */
  private async patch (ctx: ParameterizedContext, next: Next): Promise<void> {
    const user = this.userCan(Action.crud, Resource.cms)

    const body = this.bodyId(ctx)
    const cmsData = CmsModel.fromJson(body)

    const numb = await this.cmsService.patch(cmsData, user.id)

    if (numb <= 0) {
      throw new NotFound()
    }

    ctx.body = {}

    await next()
  }
}
