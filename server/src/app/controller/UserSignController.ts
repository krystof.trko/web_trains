
import { Next, ParameterizedContext } from 'koa'
import { UniqueViolationError } from 'objection'
import { ApiUrl, ConflictTypes, NotFoundTypes, UnauthorizedTypes } from 'web_trains_types'

import { Conflict } from '../exception/http/Conflict'
import { NotFound } from '../exception/http/NotFound'
import { Unauthorized } from '../exception/http/Unathorized'
import { UserModel } from '../model/UserModel'
import UserService from '../service/UserService'
import BaseController from './BaseController'

/**
 * Public routes for sign in and register
 */
export default class UserController extends BaseController {
  private userService = new UserService()

  public setRouter (): void {
    this.router.post(ApiUrl.register, this.register.bind(this))
    this.router.post(ApiUrl.signIn, this.signIn.bind(this))
  }

  /**
   * Add new user
   * @param ctx
   * @param next
   * @throws Conflict
   */
  private async register (ctx: ParameterizedContext, next: Next): Promise<void> {
    const userData = UserModel.fromJson(ctx.request.body)

    try {
      await this.userService.add(userData)
    } catch (err) {
      if (err instanceof UniqueViolationError) {
        throw new Conflict('User already exist', ConflictTypes.UserExist)
      }
    }

    ctx.body = {}

    await next()
  }

  /**
   * Sign user in
   * @param ctx
   * @param next
   * @throws NotFound
   * @throws Unauthorized
   */
  private async signIn (ctx: ParameterizedContext, next: Next): Promise<void> {
    const userBody = UserModel.fromJson(ctx.request.body)

    const userDb = await this.userService.getByName(userBody.name)

    if (userDb === undefined || userDb.password === undefined) {
      throw new NotFound('User not found: ' + userBody.name, NotFoundTypes.UserNotFound)
    }

    if (!this.userService.checkPassword(userDb.password, userBody.password)) {
      throw new Unauthorized('Bad password: ' + userBody.password + ' ' + userBody.name, UnauthorizedTypes.BadPassword)
    }

    const user = await this.userService.signIn(userDb)

    ctx.body = user

    await next()
  }
}
