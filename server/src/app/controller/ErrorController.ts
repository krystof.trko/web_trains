import { Next, ParameterizedContext } from 'koa'
import { Action, ApiUrl, LogErrorType, Resource } from 'web_trains_types'

import ClientException from '../exception/ClientException'
import { BadRequest } from '../exception/http/BadRequest'
import { NotFound } from '../exception/http/NotFound'
import ErrorService from '../service/ErrorService'
import BaseAuthController from './BaseAuthController'

/**
 * Loggin errors from client
 * and get errors
 */
export default class ErrorController extends BaseAuthController {
  private errorService = new ErrorService(this.config)

  public setRouter (): void {
    this.router.post(ApiUrl.error, this.error)

    this.router.get(ApiUrl.error, this.auth.bind(this), this.get.bind(this))
  }

  /**
   * Return message API works
   * @param ctx
   * @throws BadRequest
   */
  private async error (ctx: ParameterizedContext): Promise<void> {
    if (ctx.request.body.data === undefined) {
      throw new BadRequest()
    }

    throw new ClientException(ctx.request.body.data)
  }

  /**
   * Get errors
   * @param ctx
   * @param next
   * @throws NotFound
   */
  private async get (ctx: ParameterizedContext, next: Next): Promise<void> {
    this.userCan(Action.see, Resource.error)

    const count = Number(ctx.request.query.count)
    const offset = Number(ctx.request.query.offset)
    const type = <LogErrorType>ctx.request.query.type

    let data = null

    if (isNaN(count) && isNaN(offset)) {
      if (type === undefined) {
        throw new NotFound()
      }
      data = await this.errorService.getCount(type)
    } else if (type !== undefined) {
      data = await this.errorService.getErrors(type, count, offset)
    }

    ctx.body = data

    await next()
  }
}
