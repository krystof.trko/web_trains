import Knex from 'knex'
import { Model } from 'objection'
import { events, JourneyModel, TrainMotionInstruction } from 'web_trains_types'

import Configuration from './app/config/Configuration'
import { log } from './app/exception/Logger'
import HwBootstrap from './app/HwClientBootstrap'
import DelayService from './app/service/DelayService'
import JourneyService from './app/service/JourneyService'
import PlannerService from './app/service/PlannerService'

const config = new Configuration()

const hw = new HwBootstrap(config).run()

const knex = Knex(config.db)
Model.knex(knex)

const plannerService = new PlannerService()
const journeyService = new JourneyService()
const delayService = new DelayService()

const runningJourneys = new Set<number>()

/**
 * Get delay and assemble packet
 * @param journey
 * @returns Promise<TrainMotionInstruction|undefined>
 */
async function journeyToPacket (journey: JourneyModel): Promise<TrainMotionInstruction|undefined> {
  if (journey.train === undefined || journey.direction === undefined || journey.section === undefined || journey.stationTo === undefined) {
    return
  }

  const waitTime = await delayService.get(journey.train.id, journey.stationTo.id)

  return {
    trainMotion: {
      locomotive: journey.train.tcpName,
      speed: journey.speed,
      reverse: journey.direction
    },
    section: journey.stationTo.tcpName,
    waitTime: waitTime.delay
  }
}

async function runSequence (journeyAll: JourneyModel[]): Promise<void> {
  // TODO
  console.log(journeyAll)
}

function sendPacket (packet: TrainMotionInstruction): void {
  try {
    hw.sendPacket(events.trainMotionInstruction, packet)
  } catch (err) {
    log(config.isProduction(), config.path(Configuration.PATH_LOG), err, 'error')
  }
}

async function runAsync (journeyAll: JourneyModel[]): Promise<void> {
  for (const journey of journeyAll) {
    if (runningJourneys.has(journey.id)) {
      continue
    }

    const packet = await journeyToPacket(journey)

    if (packet === undefined) {
      continue
    }
    sendPacket(packet)
    runningJourneys.add(journey.id)
  }
}

/**
 * Get all tasks and decide to run in sequence or at once
 */
async function runTask (): Promise<void> {
  const timetableAll = await plannerService.getForDate(new Date())

  for (const timetable of timetableAll) {
    if (!timetable.journey) {
      continue
    }

    if (timetable.sequence) {
      await runSequence(timetable.journey)
    } else {
      await runAsync(timetable.journey)
    }
  }

  const journeyAll = await journeyService.getAllRunning()
  await runAsync(journeyAll)
}

hw.onData((data) => {
  const id = 0

  console.log(data)

  if (isTrainFinished()) {
    runningJourneys.delete(id)
  }
})

function isTrainFinished (): boolean {
  return false
}

setInterval(runTask, 10000) // 1min
runTask()
