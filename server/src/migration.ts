import Knex from 'knex'
import { Model } from 'objection'

import Configuration from './app/config/Configuration'

class WebpackMigrationSource {
  constructor (
        private migrationContext: __WebpackModuleApi.RequireContext
  ) {}

  getMigrations () {
    const migrations = this.migrationContext
      .keys()
      .sort()

    return Promise.resolve(migrations)
  }

  getMigrationName (migration: string) {
    return `${migration.substring(2, migration.length - 3)}.ts`
  }

  getMigration (name: string) {
    return this.migrationContext(name)
  }
}

const config = new Configuration()
const knex = Knex(config.db)
Model.knex(knex)

knex.migrate.latest({
  migrationSource: new WebpackMigrationSource(
    require.context('../migrations', false, /.ts$/)
  )
})
  .then(() => {
    console.log('Successfully migrated')
    process.exit(0)
  })
  .catch((error) => {
    console.log('Error')
    console.log(error)
    process.exit(1)
  })
