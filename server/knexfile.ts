import Configuration from './src/app/config/Configuration'

module.exports = async () => {
  return {
    ...(new Configuration()).db,
    migrations: {
      tableName: 'migrations',
      extension: 'ts'
    }
  }
}
