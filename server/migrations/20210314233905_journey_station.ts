import Knex from 'knex'

export async function up (knex: Knex): Promise<void> {
  return knex.schema.createTable('journey_station', (table) => {
    table.increments('id').primary()

    table.integer('journey_id').unsigned().nullable()
    table.foreign('journey_id').references('journey.id').onDelete('SET NULL')

    table.integer('station_id').unsigned().nullable()
    table.foreign('station_id').references('station.id').onDelete('SET NULL')
  })
}

export async function down (knex: Knex): Promise<void> {
  return knex.schema
    .dropTableIfExists('journey_station')
}
