import Knex from 'knex'

export async function up (knex: Knex): Promise<void> {
  return knex.schema.createTable('planner', (table) => {
    table.increments('id').primary()
    table.string('time').nullable()
    table.string('day').nullable()
    table.string('month').nullable()
    table.dateTime('once').nullable()

    table.integer('timetable_id').unsigned().nullable()
    table.foreign('timetable_id').references('timetable.id').onDelete('SET NULL')
  })
}

export async function down (knex: Knex): Promise<void> {
  return knex.schema
    .dropTableIfExists('planner')
}
