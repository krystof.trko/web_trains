import Knex from 'knex'

export async function up (knex: Knex): Promise<void> {
  return knex.schema.createTable('cms', (table) => {
    table.increments('id').primary()
    table.text('text')
    table.dateTime('last_changed')

    table.integer('user_id').unsigned().nullable()
    table.foreign('user_id').references('user.id').onDelete('SET NULL')
  })
}

export async function down (knex: Knex): Promise<void> {
  return knex.schema
    .dropTableIfExists('cms')
}
