import Knex from 'knex'

export async function up (knex: Knex): Promise<void> {
  return knex.schema.createTable('station', (table) => {
    table.increments('id').primary()
    table.string('name').unique()
    table.string('tcp_name').unique()
  })
}

export async function down (knex: Knex): Promise<void> {
  return knex.schema
    .dropTableIfExists('station')
}
