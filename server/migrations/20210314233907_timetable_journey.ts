import Knex from 'knex'

export async function up (knex: Knex): Promise<void> {
  return knex.schema.createTable('timetable_journey', (table) => {
    table.increments('id').primary()

    table.integer('timetable_id').unsigned().nullable()
    table.foreign('timetable_id').references('timetable.id').onDelete('SET NULL')

    table.integer('journey_id').unsigned().nullable()
    table.foreign('journey_id').references('journey.id').onDelete('SET NULL')
  })
}

export async function down (knex: Knex): Promise<void> {
  return knex.schema
    .dropTableIfExists('timetable_journey')
}
