import Knex from 'knex'

export async function up (knex: Knex): Promise<void> {
  return knex.schema.createTable('train', (table) => {
    table.increments('id').primary()
    table.string('name').unique()
    table.string('tcp_name').unique()
    table.string('img').nullable()
    table.boolean('light').nullable()
    table.integer('speed').nullable()

    table.integer('position_id').unsigned().nullable()
    table.foreign('position_id').references('station.id').onDelete('SET NULL')
  })
}

export async function down (knex: Knex): Promise<void> {
  return knex.schema
    .dropTableIfExists('train')
}
