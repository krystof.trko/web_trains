import Knex from 'knex'

export async function up (knex: Knex): Promise<void> {
  return knex.schema.createTable('user', (table) => {
    table.increments('id').primary()
    table.string('name').unique()
    table.enu('role', ['visitor', 'user', 'editor', 'admin'])
    table.string('password')
  })
}

export async function down (knex: Knex): Promise<void> {
  return knex.schema
    .dropTableIfExists('user')
}
