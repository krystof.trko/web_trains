import Knex from 'knex'

export async function up (knex: Knex): Promise<void> {
  return knex.schema.createTable('timetable', (table) => {
    table.increments('id').primary()
    table.string('name').unique()
    table.boolean('sequence')
  })
}

export async function down (knex: Knex): Promise<void> {
  return knex.schema
    .dropTableIfExists('timetable')
}
