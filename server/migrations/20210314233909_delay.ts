import Knex from 'knex'

export async function up (knex: Knex): Promise<void> {
  return knex.schema.createTable('delay', (table) => {
    table.increments('id').primary()
    table.integer('delay').unsigned()

    table.integer('train_id').unsigned().nullable()
    table.foreign('train_id').references('train.id').onDelete('SET NULL')

    table.integer('station_id').unsigned().nullable()
    table.foreign('station_id').references('station.id').onDelete('SET NULL')
  })
}

export async function down (knex: Knex): Promise<void> {
  return knex.schema
    .dropTableIfExists('delay')
}
