import Knex from 'knex'

export async function up (knex: Knex): Promise<void> {
  return knex.schema.createTable('journey', (table) => {
    table.increments('id').primary()

    table.integer('station_from_id').unsigned().nullable()
    table.foreign('station_from_id').references('station.id').onDelete('SET NULL')

    table.integer('station_to_id').unsigned().nullable()
    table.foreign('station_to_id').references('station.id').onDelete('SET NULL')

    table.integer('train_id').unsigned().nullable()
    table.foreign('train_id').references('train.id').onDelete('SET NULL')

    table.integer('speed').unsigned()
    table.boolean('running')

    table.enu('direction', ['ahead', 'reverse'])
  })
}

export async function down (knex: Knex): Promise<void> {
  return knex.schema
    .dropTableIfExists('journey')
}
