/* eslint-disable @typescript-eslint/no-var-requires */

const path = require('path')
const CopyPlugin = require('copy-webpack-plugin')
const TerserPlugin = require('terser-webpack-plugin')

module.exports = {
  entry: {
    app: './src/index.ts',
    migration: './src/migration.ts',
    cron: './src/cron.ts'
  },
  target: 'node',
  mode: 'production',
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        use: 'ts-loader',
        exclude: [
          /node_modules/
        ]
      }
    ]
  },
  resolve: {
    extensions: ['.tsx', '.ts', '.js'],
    symlinks: false
  },
  plugins: [
    new CopyPlugin({
      patterns: [
        { from: path.resolve(__dirname, 'src/config/'), to: 'config/' }
      ]
    })
  ],
  output: {
    filename: '[name].js',
    path: path.resolve(__dirname, 'dist'),
    clean: true
  },
  optimization: {
    minimizer: [
      new TerserPlugin({
        extractComments: false,
        terserOptions: {
          keep_fnames: /./ // syntax errors
        }
      })
    ]
  },
  externals: {
    sqlite3: 'sqlite3',
    // mysql2: 'mysql2',
    mariasql: 'mariasql',
    mysql: 'mysql',
    mssql: 'mssql',
    oracle: 'oracle',
    'strong-oracle': 'strong-oracle',
    oracledb: 'oracledb',
    pg: 'pg',
    'pg-query-stream': 'pg-query-stream',
    tedious: 'tedious',
    'mssql/package.json': '{}',
    'mssql/lib/base': 'mssql/lib/base'
  },
  node: {
    __dirname: false
  }
}
