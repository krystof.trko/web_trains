import Knex from 'knex'

import MigrationBootstrap from '../src/app/MigrationBootstrap'
import { StationModel } from '../src/app/model/StationModel'

/*
  { name: 'Karlstejn', tcpName: 'Karlštejn' },
  { name: 'Beroun', tcpName: 'Beroun' },
*/

const data = [
  { name: 'Širá trať levá', tcpName: 'Sira_trat_leva' },
  { name: 'Beroun kolej A', tcpName: 'Beroun_kolej_A' },
  { name: 'Karlštejn kolej A', tcpName: 'Karlstejn_kolej_A' },
  { name: 'Beroun kolej B', tcpName: 'Beroun_kolej_B' },
  { name: 'Karlštejn kolej B', tcpName: 'Karlstejn_kolej_B' },
  { name: 'Lhota kolej C', tcpName: 'Lhota_kolej_C' },
  { name: 'Beroun kolej C', tcpName: 'Beroun_kolej_C' },
  { name: 'Širá trať pravá', tcpName: 'Sira_trat_prava' }
]

export async function seed (knex: Knex): Promise<void> {
  (new MigrationBootstrap()).run()
  await knex('station').del()

  for (const val of data) {
    await StationModel.query().insert(val)
  }
}
