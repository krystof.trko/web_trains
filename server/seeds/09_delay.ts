import Knex from 'knex'

import MigrationBootstrap from '../src/app/MigrationBootstrap'
import { DelayModel } from '../src/app/model/DelayModel'

export async function seed (knex: Knex): Promise<void> {
  (new MigrationBootstrap()).run()
  await knex('delay').del()

  await DelayModel.query().insert(
    {
      trainId: 1,
      stationId: 1,
      delay: 9000
    }
  )

  await DelayModel.query().insert(
    {
      trainId: 3,
      stationId: 2,
      delay: 2000
    }
  )
}
