import Knex from 'knex'

import MigrationBootstrap from '../src/app/MigrationBootstrap'
import { TimetableModel } from '../src/app/model/TimetableModel'

export async function seed (knex: Knex): Promise<void> {
  (new MigrationBootstrap()).run()
  await knex('timetable').del()

  await TimetableModel.query().insert(
    { name: 'Timetable 1', sequence: true }
  )

  await TimetableModel.query().insert(
    { name: 'Timetable 2', sequence: false }
  )
}
