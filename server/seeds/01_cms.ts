import Knex from 'knex'

import MigrationBootstrap from '../src/app/MigrationBootstrap'
import { CmsModel } from '../src/app/model/CmsModel'

const loremIpsum = 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aliquam ornare wisi eu metus. Nulla quis diam. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos hymenaeos. Sed elit dui, pellentesque a, faucibus vel, interdum nec, diam. Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur? Pellentesque sapien. Sed vel lectus. Donec odio tempus molestie, porttitor ut, iaculis quis, sem. Integer tempor. Morbi leo mi, nonummy eget tristique non, rhoncus non leo. Pellentesque sapien. Phasellus enim erat, vestibulum vel, aliquam a, posuere eu, velit. Mauris metus. Mauris suscipit, ligula sit amet pharetra semper, nibh ante cursus purus, vel sagittis velit mauris vel metus. Duis risus. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat.'

export async function seed (knex: Knex): Promise<void> {
  (new MigrationBootstrap()).run()
  await knex('cms').del()

  await CmsModel.query().insert({
    text: loremIpsum,
    userId: 1
  })
}
