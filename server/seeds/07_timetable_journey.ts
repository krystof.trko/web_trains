import Knex from 'knex'

export async function seed (knex: Knex): Promise<void> {
  await knex('timetable_journey').del()

  await knex('timetable_journey').insert(
    { timetable_id: 1, journey_id: 1 }
  )

  await knex('timetable_journey').insert(
    { timetable_id: 2, journey_id: 1 }
  )
}
