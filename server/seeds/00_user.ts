import Knex from 'knex'
import { Role } from 'web_trains_types'

import MigrationBootstrap from '../src/app/MigrationBootstrap'
import { UserModel } from '../src/app/model/UserModel'
import UserService from '../src/app/service/UserService'

export async function seed (knex: Knex): Promise<void> {
  (new MigrationBootstrap()).run()

  await knex('user').del()

  const service = new UserService()

  const model = UserModel.fromJson({
    name: 'admin', password: 'admin1234'
  })

  await service.add(model)

  const patch = {
    id: model.id,
    role: Role.admin
  }

  await service.patch(UserModel.fromJson(patch))
}
