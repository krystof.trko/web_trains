import Knex from 'knex'

import MigrationBootstrap from '../src/app/MigrationBootstrap'
import { TrainModel } from '../src/app/model/TrainModel'

const data = [
  { name: 'Brejlovec', tcpName: 'Brejlovec', img: 'Brejlovec.jpg', light: false, speed: 0, positionId: null },
  { name: 'Desiero (Kamera)', tcpName: 'Desiero_(Kamera)', img: null, light: false, speed: 0, positionId: null },
  { name: 'Taurus Railion', tcpName: 'Taurus_Railion', img: null, light: false, speed: 0, positionId: null },
  { name: 'DB204 274-5', tcpName: 'DB204_274-5', img: 'Desiero_DB642.jpg', light: false, speed: 0, positionId: null },
  { name: 'Ragulin', tcpName: 'Ragulin', img: 'Ragulin.jpg', light: false, speed: 0, positionId: null },
  { name: 'ICE', tcpName: 'ICE', img: 'ICE.jpg', light: false, speed: 0, positionId: null },
  { name: 'Taurus DHL', tcpName: 'Taurus_DHL', img: 'Taurus_DHL.jpg', light: false, speed: 0, positionId: null },
  { name: 'Herkules Priessnitz', tcpName: 'Herkules_Priessnitz', img: null, light: false, speed: 0, positionId: null },
  { name: 'Para 555', tcpName: 'Para_555', img: null, light: false, speed: 0, positionId: null },
  { name: 'T334 Rosnicka', tcpName: 'T334_Rosnicka', img: null, light: false, speed: 0, positionId: null },
  { name: 'Desiero DB642 133-3', tcpName: 'Desiero_DB642_133-3', img: null, light: false, speed: 0, positionId: null },
  { name: 'Taurus EVB', tcpName: 'Taurus_EVB', img: 'Taurus_EVB.jpg', light: false, speed: 0, positionId: null },
  { name: 'ES363', tcpName: 'ES363', img: null, light: false, speed: 0, positionId: null }
]

export async function seed (knex: Knex): Promise<void> {
  (new MigrationBootstrap()).run()
  await knex('train').del()

  for (const val of data) {
    await TrainModel.query().insert(val)
  }
}
