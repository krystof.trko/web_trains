import Knex from 'knex'
import { JourneyDirection } from 'web_trains_types'

import MigrationBootstrap from '../src/app/MigrationBootstrap'
import { JourneyModel } from '../src/app/model/JourneyModel'

export async function seed (knex: Knex): Promise<void> {
  (new MigrationBootstrap()).run()
  await knex('journey').del()

  await JourneyModel.query().insert({
    stationFromId: 2,
    stationToId: 1,
    trainId: 1,
    speed: 22,
    direction: JourneyDirection.ahead,
    running: false
  })

  await JourneyModel.query().insert({
    stationFromId: 2,
    stationToId: 2,
    trainId: 2,
    direction: JourneyDirection.reverse,
    running: false
  })
}
