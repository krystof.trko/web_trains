import Knex from 'knex'

export async function seed (knex: Knex): Promise<void> {
  await knex('journey_station').del()

  await knex('journey_station').insert([
    { journey_id: 1, station_id: 1 }
  ])
}
