import Knex from 'knex'

import MigrationBootstrap from '../src/app/MigrationBootstrap'
import { PlannerModel } from '../src/app/model/PlannerModel'

export async function seed (knex: Knex): Promise<void> {
  (new MigrationBootstrap()).run()
  await knex('planner').del()

  await PlannerModel.query().insert(
    {
      time: [{ h: 10, m: 20 }, { h: 20, m: 20 }],
      day: [0, 2, 3, 4, 5],
      month: [0, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11],
      timetableId: 1
    }
  )

  await PlannerModel.query().insert(
    {
      once: '2021-04-09 10:00:00',
      timetableId: 2
    }
  )
}
