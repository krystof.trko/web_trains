export enum ApiUrl {
  test = '/test',
  register = '/register',
  signIn = '/signin',
  me = '/me',
  user = '/user',
  error = '/error',
  cms = '/cms',
  journey = '/journey',
  planner = '/planner',
  timetable = '/timetable',
  station = '/station',
  train = '/train',
  delay = '/delay'
}
