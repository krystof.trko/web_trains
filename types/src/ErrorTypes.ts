export enum BadRequestTypes {
  Validation,
  DbNotNullViolation,
  DbCheckViolation,
  DbData,
  DbConstraintViolationError
}

export enum ConflictTypes {
  DbUniqueViolation,
  DbForeignKeyViolation,
  UserExist
}

export enum NotFoundTypes {
  DbNotFound,
  UserNotFound
}

export enum UnauthorizedTypes {
  TokenExpired,
  DeletedUser,
  BadPassword
}

export enum HardwareTypes {
  NotConnected,
  WriteError
}

export interface ExceptionBody {
  message: string
  type: BadRequestTypes | ConflictTypes | NotFoundTypes | UnauthorizedTypes | HardwareTypes | null
  code: number
}
