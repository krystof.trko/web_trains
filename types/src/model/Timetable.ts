import { JourneyModel } from './Journey'

export interface TimetableAdd {
  name: string
  journey: number[]
  sequence: boolean
}

export interface TimetablePatch {
  name?: string
  sequence?: boolean
  journey?: number[]
}

export interface TimetableModel {
  id: number
  name: string
  sequence: boolean
  journey?: JourneyModel[]
}
