import { StationModel } from './Station'
import { TrainModel } from './Train'

export interface DelayModel {
  id: number
  trainId: number|null
  train?: TrainModel
  stationId: number|null
  station?: StationModel
  delay: number
}

export interface DelayPut {
  trainId: number|null
  stationId: number|null
  delay: number
}
