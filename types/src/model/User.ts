export enum Role {
    visitor = 'visitor',
    user = 'user',
    editor = 'editor',
    admin = 'admin'
}

export interface UserModel {
  id: number
  name: string
  role: Role
}

export interface UserPatch {
  name?: string
  role?: Role
  password?: string
}

export interface UserSignInRes {
  token: string
}

export interface UserCredential {
  name: string,
  password: string
}
