export interface StationModel {
  id: number
  name: string
  tcpName: string
}
