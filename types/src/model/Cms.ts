import { UserModel } from './User'

export interface CmsModel {
  id: number
  text: string
  lastChanged: string,
  userId?: number|null,
  user?: UserModel
}

export interface CmsPatch {
  text: string
}
