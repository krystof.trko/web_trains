import { ExceptionBody } from '../ErrorTypes'

export enum LogErrorType {
  client = 'client',
  error = 'error',
  info = 'info',
  hardware = 'hardware'
}

export interface LogErrorRow {
  timestamp: string,
  level: string,
  message: string,
  body?: ExceptionBody,
  status?: number,
  service?: string,
  stack?: string
}

export interface LogErrorReq {
  type: LogErrorType,
  count: number,
  offset: number
}
