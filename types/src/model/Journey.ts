import { StationModel } from './Station'
import { TrainModel } from './Train'

export enum JourneyDirection {
  reverse = 'reverse',
  ahead = 'ahead'
}

export interface JourneyPatch {
  stationFromId?: number|null
  stationToId?: number|null
  trainId?: number|null
  speed?: number
  section?: number[]
  direction?: JourneyDirection,
  running?: boolean
}

export interface JourneyAdd {
  stationFromId: number|null
  stationToId: number|null
  trainId: number|null
  speed: number
  section: number[]
  direction: JourneyDirection
}

export interface JourneyModel {
  id: number
  stationFromId?: number|null
  stationFrom?: StationModel
  stationToId?: number|null
  stationTo?: StationModel
  trainId?: number|null
  train?: TrainModel
  speed: number
  section?: StationModel[]
  direction?: JourneyDirection,
  running: boolean
}
