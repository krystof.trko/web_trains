import { StationModel } from './Station'

export interface TrainModel {
  id: number
  name: string
  tcpName: string
  img: string
  light: boolean
  speed: number
  positionId: number|null
  position?: StationModel
}

export interface AllRequest {
  onlyWithPos?: boolean
}

export interface SetPositionRequest {
  trainId: number
  positionId: number|null
}
