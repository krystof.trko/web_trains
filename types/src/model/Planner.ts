import { TimetableModel } from './Timetable'

export interface PlannerTime {
  h: number,
  m: number
}
export interface PlannerPatch {
  time?: PlannerTime[]|null
  day?: number[]|null
  month?: number[]|null
  once?: string|Date|null
  timetableId?: number|null
}
export interface PlannerAdd {
  time: PlannerTime[]|null
  day: number[]|null
  month: number[]|null
  once: string|Date|null
  timetableId: number|null
}
export interface PlannerModel {
  id: number
  time: PlannerTime[]|null
  day: number[]|null
  month: number[]|null
  once: string|Date|null
  timetableId?: number|null
  timetable?: TimetableModel|null
}
