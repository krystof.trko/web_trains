import { JourneyDirection } from '../model/Journey'

// types for creating tcp request
export enum unitInstruction {
  currentDelay = 'prodleva_odesilani_zmerenych_proudu',
  powerOn = 'zapnuti_zdroje',
  powerOff = 'vypnuti_zdroje',
  resetRectifier = 'restart_h_mustku',
  resetProcesor = 'restart_mikroprocesoru',
  err = 'err',
}

export enum lightState {
  on = 'on',
  off = 'off'
}

// REQUESTS
export interface UnitInstruction {
  unitInstruction: unitInstruction
  numberOfUnit: number
  currentDelay?: number
}

export interface TrainMotion {
  locomotive: string
  speed: number
  reverse: JourneyDirection
  trainId?: number // from client
}

export interface TrainMotionInstruction {
  trainMotion: TrainMotion
  section: string
  waitTime: number
}

export interface TrainFunction {
  locomotive: string
  lights: boolean
  trainId?: number // from client
}

export interface UnitInfo {
  unitInfo: unitInstruction
  numberOfUnit: number
}

export interface OledInfo {
  messages: string[]
}

// RESPONSES

export interface SectionData {
  current: number
  occupancy: boolean
  name: string
}
export interface OccupancySection {
  sections: SectionData[]
}

// Transfer - no hw
export interface TrainPosition {
  t: number, // trainId
  p: number|null // positionId
}

// All packet types and number for runJourney and runTimetable
export type Packets = UnitInstruction|TrainMotion|TrainMotionInstruction|TrainFunction|UnitInfo|OledInfo|OccupancySection|TrainPosition|number
