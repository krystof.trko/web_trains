export enum Namespace {
  adminHardware = 'admin_hardware',
  adminMap = 'admin_map',
  publicMap = 'public_map'
}
