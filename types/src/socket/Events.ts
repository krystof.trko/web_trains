// Used for serializing data to hw
export enum events {
  unitInstruction = 'unit_instruction',
  trainMotion = 'train_move',
  trainMotionInstruction = 'train_move_to_place',
  trainFunction = 'train_function',
  unitInfo = 'unit_info',
  oledInfo = 'oled_info',
  occupancySection = 'occupancy_section'
}

// To transfer data between client and server
export enum eventTransfer {
  runJourney= 'addJourney',
  runTimetable = 'addTimetable',
  positions = 'positions'
}
