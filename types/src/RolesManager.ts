import { Role } from './model/User'

export enum Action {
  see,
  crud
}

export enum Resource {
  userAdmin,
  timetable,
  cms,
  error,
  hardwareAdmin
}

interface RolesTable {
  [Role: string]: {
    [Resource: number]: Action[]
  }
}

/**
 * Define user roles
 * visitor - default role of new user, for future usage
 * user - can run timetables
 * editor - user + can create timetables
 * admin - editor + can changes users + see errors
 */
export class RolesManager {
  private roles: RolesTable

  public constructor () {
    this.roles = {
      [Role.user]: {
        [Resource.timetable]: [Action.see]
      },

      [Role.editor]: {
        [Resource.timetable]: [Action.see, Action.crud],
        [Resource.cms]: [Action.see, Action.crud],
        [Resource.error]: [Action.see],
        [Resource.hardwareAdmin]: [Action.see, Action.crud]
      },

      [Role.admin]: {
        [Resource.timetable]: [Action.see, Action.crud],
        [Resource.userAdmin]: [Action.see, Action.crud],
        [Resource.cms]: [Action.see, Action.crud],
        [Resource.error]: [Action.see, Action.crud],
        [Resource.hardwareAdmin]: [Action.see, Action.crud]
      }
    }
  }

  /**
   * Is role allowed to work with resoruce.
   * @param role
   * @param action
   * @param resource
   * @returns boolean
   */
  public can (role: Role, action: Action, resource: Resource): boolean {
    if (this.roles[role] === undefined || !Array.isArray(this.roles[role][resource])) {
      return false
    }

    return this.roles[role][resource].includes(action)
  }

  /**
   * Has role permissions to more actions on resource
   * @param role
   * @param actionMore
   * @param resource
   * @returns boolean
   */
  public canMore (role: Role, actionMore: Action[], resource: Resource): boolean {
    let allowedAllActions = true

    actionMore.forEach((action) => {
      allowedAllActions &&= this.can(role, action, resource)
    })

    return allowedAllActions
  }
}
