export enum HttpTypes {
  BadRequest = 400,
  Conflict = 409,
  Forbidden = 403,
  MethodNotAllowed = 405,
  NotFound = 404,
  Unauthorized = 401,
  UnprocessableEntity = 422,
  ServerError = 500
}
