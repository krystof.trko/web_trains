#!/bin/sh
npm run --prefix types build
npm run --prefix client build
npm run --prefix server build

rm -rf dist/*

cp -r server/dist/* dist
mkdir dist/client
cp -r client/dist/* dist/client

mkdir -p dist/log/nginx
mkdir -p dist/log/pm2
mkdir -p dist/log/nginx
mkdir -p dist/log/web_trains
mkdir -p dist/adminer
mkdir -p dist/analytics

cp .gitlab-ci-dist.yml dist/gitlab-ci.yml