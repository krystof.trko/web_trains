#!/bin/sh
if [ $# -eq 0 ]; then
	eval `ssh-agent`
	ssh-add ~/.ssh/id_rsa
	docker-compose up -d

	GIT_NAME=$(git config --get --global user.name)
	GIT_EMAIL=$(git config --get --global user.email)

	docker exec -it trains_webserver git config --global user.email "$GIT_EMAIL"
	docker exec -it trains_webserver git config --global user.name "$GIT_NAME"
	docker exec -it trains_webserver bash
elif [ $1 == "prod" ]; then
    docker-compose -f docker-compose.prod.yml up -d
	docker exec -it trains_prod_webserver bash
fi
