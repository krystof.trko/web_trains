export enum Route {
  home = 'home',
  about = 'about',
  attribution = 'attribution',
  signIn = 'signIn',
  register = 'register',
  user = 'user',
  signOut = 'signOut',
  adminUser = 'adminUser',
  notFound = 'notFound',
  timetable = 'timetable',
  adminTimetable = 'adminTimetable',
  adminError = 'adminError',
  adminHardware = 'adminHardware'
}
