import { createRouter, createWebHistory } from 'vue-router'

import Authorization from '@/router/authorization'
import { Route } from '@/router/routes'

const routes = [
  {
    path: '/',
    name: Route.home,
    component: () => import(/* webpackChunkName: "home" */ '@/pages/Home.vue')
  },
  {
    path: '/o-kolejisti',
    name: Route.about,
    component: () => import(/* webpackChunkName: "about" */ '@/pages/About.vue')
  },
  {
    path: '/zdroje',
    name: Route.attribution,
    component: () => import(/* webpackChunkName: "attribution" */ '@/pages/Attribution.vue')
  },
  {
    path: '/prihlasit',
    name: Route.signIn,
    component: () => import(/* webpackChunkName: "signIn" */ '@/pages/User/SignIn.vue')
  },
  {
    path: '/registrovat',
    name: Route.register,
    component: () => import(/* webpackChunkName: "register" */ '@/pages/User/Register.vue')
  },
  {
    path: '/uzivatel',
    name: Route.user,
    component: () => import(/* webpackChunkName: "user" */ '@/pages/User/User.vue')
  },
  {
    path: '/odhlasit',
    name: Route.signOut,
    component: () => import(/* webpackChunkName: "signOut" */ '@/pages/User/SignOut.vue')
  },
  {
    path: '/admin/uzivatel',
    name: Route.adminUser,
    component: () => import(/* webpackChunkName: "adminUser" */ '@/pages/Admin/AdminUser.vue')
  },
  {
    path: '/jizdni-rady',
    name: Route.timetable,
    component: () => import(/* webpackChunkName: "timeTable" */ '@/pages/Planner.vue')
  },
  {
    path: '/admin/jizdni-rady',
    name: Route.adminTimetable,
    component: () => import(/* webpackChunkName: "adminTimetable" */ '@/pages/Admin/AdminTimetable.vue')
  },
  {
    path: '/admin/chyby',
    name: Route.adminError,
    component: () => import(/* webpackChunkName: "adminError" */ '@/pages/Admin/AdminError.vue')
  },
  {
    path: '/admin/hardware',
    name: Route.adminHardware,
    component: () => import(/* webpackChunkName: "adminHardware" */ '@/pages/Admin/AdminHardware.vue')
  },
  {
    path: '/:pathMatch(.*)*',
    name: Route.notFound,
    component: () => import(/* webpackChunkName: "notFound" */ '@/pages/NotFound.vue')
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

// Validate access
router.beforeEach((to, from) => {
  const auth = new Authorization(<Route> to.name, from, router)
  return auth.authorize()
})

export default router
