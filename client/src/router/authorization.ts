import { Notify } from 'quasar'
import { RouteLocationNormalized, Router } from 'vue-router'
import { Action, Resource, Role, RolesManager } from 'web_trains_types'

import { Route } from '@/router/routes'
import useUserStore from '@/store/userStore'

/**
 * Only not logged user can acess
 */
const onlyNotLoggedUrls = [
  Route.signIn, Route.register
]

interface UrlResource {
  [Route: string]: {
    [Resource: number]: Action[]
  }
}

/**
 * Route has resource and user must have permission to all actions
 * Only logged user can access
 */
const urlResource: UrlResource = {
  [Route.user]: {

  },
  [Route.signOut]: {

  },
  [Route.adminUser]: {
    [Resource.userAdmin]: [
      Action.crud
    ]
  },
  [Route.adminTimetable]: {
    [Resource.timetable]: [
      Action.see,
      Action.crud
    ]
  },
  [Route.adminError]: {
    [Resource.error]: [
      Action.see,
      Action.crud
    ]
  },
  [Route.adminHardware]: {
    [Resource.error]: [
      Action.see,
      Action.crud
    ]
  }
}

export default class Authorization {
  constructor (private to: Route, private from: RouteLocationNormalized, private router: Router) {
  }

  /**
   * Check if user is authorized to acess route
   * @returns boolean
   */
  public authorize (): boolean {
    const userStore = useUserStore()

    if (userStore.logged) {
      return this.isLogged(<Role> userStore.data.role)
    }

    return this.isNotLogged()
  }

  /**
   * If user is logged and route is in onlyNotLoggedUrls go back
   * @param role
   * @returns boolean
   */
  private isLogged (role: Role): boolean {
    if (onlyNotLoggedUrls.includes(this.to)) {
      return this.sendBack('Musíte se nejdříve odhlásit')
    }

    // route is in urlResource => is not public
    if (this.to in urlResource) {
      return this.loggedAccessResource(role)
    }

    // Routes not in urlResource are public
    return true
  }

  /**
   * Check if current user can acess route
   * @param role
   * @returns boolean
   */
  private loggedAccessResource (role: Role): boolean {
    const rolesManager = new RolesManager()
    const resource = Object.keys(urlResource[this.to])

    // Has no resources
    if (resource.length === 0) {
      return true
    }

    let allowedAllActions = true

    resource.forEach((item) => {
      const resource = Number(item)
      const actionArr = urlResource[this.to][resource]
      allowedAllActions &&= rolesManager.canMore(role, actionArr, resource)
    })

    return allowedAllActions ? true : this.sendBack('Nemáte oprávnění')
  }

  /**
   * If route is in urlResource and user is not logged go back
   * @returns boolean
   */
  private isNotLogged (): boolean {
    return this.to in urlResource ? this.sendBack('Přihlašte se prosím') : true
  }

  /**
   * Send user to home and return false
   * @returns boolean
   */
  private sendBack (message: string): boolean {
    Notify.create({
      type: 'negative',
      message: message
    })

    this.router.push({ name: Route.home })

    return false
  }
}
