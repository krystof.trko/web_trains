import { BadRequestTypes, ConflictTypes, ExceptionBody, HardwareTypes, HttpTypes, NotFoundTypes, UnauthorizedTypes } from 'web_trains_types'

export const translateDefault = 'Chyba připojení k serveru'

const translateTableDefaults = {
  [HttpTypes.BadRequest]: 'Špatný dotaz',
  [HttpTypes.Conflict]: 'Konflikt',
  [HttpTypes.Forbidden]: 'Nemáte oprávnění',
  [HttpTypes.MethodNotAllowed]: 'Metoda není povolena',
  [HttpTypes.NotFound]: 'Nenalezeno',
  [HttpTypes.Unauthorized]: 'Přihlašte se prosím',
  [HttpTypes.UnprocessableEntity]: 'Nelze zpracovat',
  [HttpTypes.ServerError]: 'Chyba serveru'
}

interface table {
  [index: number]: {
    [index: number]: string
  }
}

const translateTable: table = {
  [HttpTypes.BadRequest]: {
    [BadRequestTypes.Validation]: 'Chyba validace',
    [BadRequestTypes.DbNotNullViolation]: 'Chyba databáze - Null',
    [BadRequestTypes.DbCheckViolation]: 'Chyba databáze - Check',
    [BadRequestTypes.DbData]: 'Chyba databáze - Data',
    [BadRequestTypes.DbConstraintViolationError]: 'Chyba databáze - ConstraintViolation'
  },

  [HttpTypes.Conflict]: {
    [ConflictTypes.DbUniqueViolation]: 'Již existuje',
    [ConflictTypes.DbForeignKeyViolation]: 'Chyba databáze - ForeignKey',
    [ConflictTypes.UserExist]: 'Uživatelské jméno již existuje'
  },

  [HttpTypes.Forbidden]: {
  },

  [HttpTypes.MethodNotAllowed]: {
  },

  [HttpTypes.NotFound]: {
    [NotFoundTypes.DbNotFound]: 'Nenalezeno',
    [NotFoundTypes.UserNotFound]: 'Uživatel neexistuje'
  },

  [HttpTypes.Unauthorized]: {
    [UnauthorizedTypes.TokenExpired]: 'Prosím znovu se přihlašte',
    [UnauthorizedTypes.DeletedUser]: 'Uživatel byl smazán',
    [UnauthorizedTypes.BadPassword]: 'Špatné heslo'
  },

  [HttpTypes.UnprocessableEntity]: {
  },

  [HttpTypes.ServerError]: {
    [HardwareTypes.NotConnected]: 'Hardware není připojen',
    [HardwareTypes.WriteError]: 'Hardware: nelze poslat data'
  }
}

type AxiosError = {
  status?: HttpTypes|number,
  data?: ExceptionBody, // axios
  body?: ExceptionBody // socket
}

export default function (response: undefined|AxiosError): ExceptionBody {
  const data = response?.data ?? response?.body

  if (response === undefined || response.status === undefined || data === undefined || !(response.status in translateTable)) {
    return {
      message: translateDefault,
      type: null,
      code: 0
    }
  }

  const status:HttpTypes = response.status

  if (data.type === null) {
    return {
      message: translateTableDefaults[status],
      type: null,
      code: status
    }
  }

  if (data.type in translateTable[status]) {
    return {
      message: translateTable[status][data.type],
      type: data.type,
      code: status
    }
  }

  return {
    message: translateDefault,
    type: null,
    code: status
  }
}
