import { Role } from 'web_trains_types'

export const roleTranslated = {
  [Role.visitor]: 'Návštěvník',
  [Role.user]: 'Uživatel',
  [Role.editor]: 'Editor',
  [Role.admin]: 'Administrátor'
}

export const roleTranslatedSelect = [
  {
    value: Role.visitor,
    label: roleTranslated[Role.visitor]
  },
  {
    value: Role.user,
    label: roleTranslated[Role.user]
  },
  {
    value: Role.editor,
    label: roleTranslated[Role.editor]
  },
  {
    value: Role.admin,
    label: roleTranslated[Role.admin]
  }
]
