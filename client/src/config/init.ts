import { Dialog, Loading, Notify } from 'quasar'
import lang from 'quasar/lang/cs.js'

Notify.setDefaults({
  position: 'top',
  progress: true,
  timeout: 5000,
  textColor: 'white',
  actions: [{ icon: 'close', color: 'white' }]
})

export const quasarUserOptions = {
  plugins: {
    Notify,
    Loading,
    Dialog
  },
  config: {
  },
  lang: lang
}
