import ApiService from '@/service/ApiService'
import AdminHardwareService from '@/service/resource/AdminHardwareService'
import AdminMapService from '@/service/resource/AdminMapService'
import CmsService from '@/service/resource/CmsService'
import DelayService from '@/service/resource/DelayService'
import ErrorService from '@/service/resource/ErrorService'
import JourneyService from '@/service/resource/JourneyService'
import MapService from '@/service/resource/MapService'
import PlannerService from '@/service/resource/PlannerService'
import StationService from '@/service/resource/StationService'
import TimetableService from '@/service/resource/TimetableService'
import TrainService from '@/service/resource/TrainService'
import UserService from '@/service/resource/UserService'
import SocketService from '@/service/SocketService'
import StorageService from '@/service/StorageService'

// Instances
export let userService: UserService
export let cmsService: CmsService
export let delayService: DelayService
export let mapService: MapService
export let adminHardwareService: AdminHardwareService
export let adminMapService: AdminMapService
export let journeyService: JourneyService
export let plannerService: PlannerService
export let stationService: StationService
export let timetableService: TimetableService
export let trainService: TrainService
export let errorService: ErrorService
export let socketService: SocketService

const storage = new StorageService()

export function bootstrapSocket (): void {
  // Base services
  socketService = new SocketService(storage)

  // Resource publicMap
  mapService = new MapService(socketService)

  // Resource adminHardware
  adminHardwareService = new AdminHardwareService(socketService)

  // Resource adminMap
  adminMapService = new AdminMapService(socketService)
}

export function bootstrapApi (): void {
  // Base services
  const api = new ApiService()

  api.test()
    .then()

  // Resource user
  userService = new UserService(storage, api)

  // Resource cms
  cmsService = new CmsService(api)

  // Resource timetable
  journeyService = new JourneyService(api)
  plannerService = new PlannerService(api)
  stationService = new StationService(api)
  timetableService = new TimetableService(api)
  trainService = new TrainService(api)

  // Resource error
  errorService = new ErrorService(api)

  // Resource adminMap
  delayService = new DelayService(api)
}
