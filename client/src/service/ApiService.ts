import axios, { AxiosResponse } from 'axios'
import axiosRetry from 'axios-retry'
import { ApiUrl } from 'web_trains_types'

import { translateDefault } from '@/config/errorTranslator'
import { axiosError } from '@/exception/ExceptionHandler'

export default class ApiService {
  private axios

  constructor () {
    this.axios = axios.create({
      baseURL: process.env.VUE_APP_API_URL,
      timeout: 10000
    })

    axiosRetry(this.axios, { retries: 10, retryDelay: axiosRetry.exponentialDelay })
    this.axios.interceptors.response.use(response => response, axiosError)
  }

  public async get<T = any, R = AxiosResponse<T>> (url: ApiUrl, ...urlParams: string[]|number[]): Promise<R> {
    let paramsUrl = <string>url
    paramsUrl += urlParams === [] ? '' : '/' + urlParams.join('/')

    return this.axios.get(paramsUrl)
  }

  public async getParams<T = any, R = AxiosResponse<T>> (url: ApiUrl, params: any): Promise<R> {
    return this.axios.get(url, {
      params: params
    })
  }

  public async post<T = any, R = AxiosResponse<T>> (url: ApiUrl, data?: any|undefined, id?:number): Promise<R> {
    const urlId = id === undefined ? url : url + '/' + id
    return this.axios.post(urlId, data)
  }

  public async patch<T = any, R = AxiosResponse<T>> (url: ApiUrl, data?: any|undefined, id?:number): Promise<R> {
    const urlId = id === undefined ? url : url + '/' + id
    return this.axios.patch(urlId, data)
  }

  public async put<T = any, R = AxiosResponse<T>> (url: ApiUrl, data?: any|undefined, id?:number): Promise<R> {
    const urlId = id === undefined ? url : url + '/' + id
    return this.axios.put(urlId, data)
  }

  public async del<T = any, R = AxiosResponse<T>> (url: ApiUrl, id?:number): Promise<R> {
    const urlId = id === undefined ? url : url + '/' + id
    return this.axios.delete(urlId)
  }

  public setAuth (token: string): void {
    this.axios.defaults.headers.common = { Authorization: `bearer ${token}` }
  }

  public delAuth (): void {
    delete this.axios.defaults.headers.common
  }

  public async test (): Promise<void> {
    return this.get(ApiUrl.test).then((response) => {
      const ok = response.data.message === 'API works'

      return ok ? Promise.resolve() : Promise.reject(new Error(translateDefault))
    })
  }
}
