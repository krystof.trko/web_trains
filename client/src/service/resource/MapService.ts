import { eventTransfer, Namespace, TrainPosition } from 'web_trains_types'

import BaseSocketService from '@/service/BaseSocketService'

export default class MapService extends BaseSocketService {
  protected setNamespace (): Namespace {
    return Namespace.publicMap
  }

  protected setAuth (): boolean {
    return false
  }

  public trainPositions (callback: (positions: TrainPosition[]) => void): void {
    this.on(eventTransfer.positions, callback)
  }
}
