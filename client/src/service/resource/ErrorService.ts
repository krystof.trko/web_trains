import { ApiUrl, LogErrorReq, LogErrorRow, LogErrorType } from 'web_trains_types'

import ApiService from '@/service/ApiService'

export default class ErrorService {
  constructor (private api: ApiService) {

  }

  public async getAll (params: LogErrorReq): Promise<LogErrorRow[]> {
    return this.api.getParams(ApiUrl.error, params)
      .then((resp) => resp.data)
  }

  public async getCount (type: LogErrorType): Promise<number> {
    return this.api.getParams(ApiUrl.error, {
      type: type
    })
      .then((resp) => resp.data)
  }
}
