import { events, eventTransfer, JourneyDirection, Namespace } from 'web_trains_types'

import BaseSocketService from '@/service/BaseSocketService'

export default class AdminMapService extends BaseSocketService {
  protected setNamespace (): Namespace {
    return Namespace.adminMap
  }

  protected setAuth (): boolean {
    return true
  }

  public async lights (lights: boolean, locomotive: string, trainId: number): Promise<boolean> {
    return this.emit(events.trainFunction, {
      lights: lights,
      locomotive: locomotive,
      trainId: trainId
    })
  }

  public async speed (speed: number, locomotive: string, trainId: number): Promise<boolean> {
    let reverse = JourneyDirection.ahead

    if (speed < 0) {
      reverse = JourneyDirection.reverse
    }

    return this.emit(events.trainMotion, {
      speed: speed,
      reverse: reverse,
      locomotive: locomotive,
      trainId: trainId
    })
  }

  public async runJourney (id: number): Promise<boolean> {
    return this.emit(eventTransfer.runJourney, id)
  }

  public async runTimetable (id: number): Promise<boolean> {
    return this.emit(eventTransfer.runTimetable, id)
  }
}
