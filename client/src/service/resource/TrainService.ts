import { AllRequest, ApiUrl, SetPositionRequest, TrainModel } from 'web_trains_types'

import ApiService from '@/service/ApiService'

export default class TrainService {
  constructor (private api: ApiService) {

  }

  public async getAll (params?: AllRequest): Promise<TrainModel[]> {
    return this.api.getParams(ApiUrl.train, params)
      .then((resp) => resp.data)
  }

  public async setPosition (data: SetPositionRequest): Promise<TrainModel[]> {
    const id = data.trainId

    const partial = data as Partial<SetPositionRequest>
    delete partial.trainId

    return this.api.patch(ApiUrl.train, partial, id)
  }
}
