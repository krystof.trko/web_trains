import { formatISO } from 'date-fns'
import { ApiUrl, PlannerAdd, PlannerModel, PlannerPatch } from 'web_trains_types'

import ApiService from '@/service/ApiService'

export default class PlannerService {
  constructor (private api: ApiService) {

  }

  public async get (id: number): Promise<PlannerModel> {
    return this.api.get(ApiUrl.planner, id)
      .then((resp) => resp.data)
  }

  public async getAll (): Promise<PlannerModel[]> {
    return this.api.get(ApiUrl.planner)
      .then((resp) => resp.data)
  }

  public async getRange (from: Date, to: Date): Promise<PlannerModel[]> {
    const fromTxt = formatISO(from)
    const toTxt = formatISO(to)

    return this.api.get(ApiUrl.planner, 'range', fromTxt, toTxt)
      .then((resp) => resp.data)
  }

  public async add (data: PlannerAdd): Promise<PlannerModel> {
    return this.api.post(ApiUrl.planner, data)
      .then((resp) => resp.data)
  }

  public async patch (id: number, data: PlannerPatch): Promise<void> {
    return this.api.patch(ApiUrl.planner, data, id)
  }

  public async del (id: number): Promise<void> {
    return this.api.del(ApiUrl.planner, id)
  }
}
