import { events, Namespace, UnitInstruction, unitInstruction } from 'web_trains_types'

import BaseSocketService from '@/service/BaseSocketService'

export default class AdminHardwareService extends BaseSocketService {
  protected setNamespace (): Namespace {
    return Namespace.adminHardware
  }

  protected setAuth (): boolean {
    return true
  }

  public unitInstruction (instruction: unitInstruction, unit: number, delay?: number): Promise<boolean> {
    const data:UnitInstruction = {
      unitInstruction: instruction,
      numberOfUnit: unit
    }

    if (delay !== undefined) {
      data.currentDelay = delay
    }

    return this.emit(events.unitInstruction, data)
  }
}
