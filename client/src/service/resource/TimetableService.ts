import { ApiUrl, TimetableAdd, TimetableModel, TimetablePatch } from 'web_trains_types'

import ApiService from '@/service/ApiService'

export default class TimetableService {
  constructor (private api: ApiService) {

  }

  public async get (id: number): Promise<TimetableModel> {
    return this.api.get(ApiUrl.timetable, id)
      .then((resp) => resp.data)
  }

  public async getAll (): Promise<TimetableModel[]> {
    return this.api.get(ApiUrl.timetable)
      .then((resp) => resp.data)
  }

  public async add (data: TimetableAdd): Promise<TimetableModel> {
    return this.api.post(ApiUrl.timetable, data)
      .then((resp) => resp.data)
  }

  public async patch (id: number, data: TimetablePatch): Promise<void> {
    return this.api.patch(ApiUrl.timetable, data, id)
  }

  public async del (id: number): Promise<void> {
    return this.api.del(ApiUrl.timetable, id)
  }
}
