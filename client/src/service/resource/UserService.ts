import { ApiUrl, UserCredential, UserModel, UserPatch } from 'web_trains_types'

import ApiService from '@/service/ApiService'
import StorageService from '@/service/StorageService'

export default class UserService {
  constructor (private storage: StorageService, private api: ApiService) {

  }

  /**
   * If user is stored set axios header and return user data
   * @returns UserModel|null
   */
  public setAuth (): UserModel|null {
    const jwt = this.storage.getJwt()

    if (jwt !== null) {
      this.api.setAuth(jwt)
      return this.storage.getData()
    }

    return null
  }

  /**
   * Get current logged user and save received data
   * @returns Promise<UserModel>
   */
  public async getMe (): Promise<UserModel> {
    return this.api
      .get<UserModel>(ApiUrl.me)
      .then(response => {
        this.storage.setData(response.data)
        return response.data
      })
  }

  /**
   * Get JWT, save it and after get data
   * @param data
   * @returns Promise<UserModel>
   */
  public async signIn (data: UserCredential): Promise<UserModel> {
    return this.api
      .post(ApiUrl.signIn, data)
      .then(response => {
        this.storage.setJwt(response.data.token)
        this.api.setAuth(response.data.token)

        return this.getMe()
      })
  }

  /**
   * Send new user data
   * @param data
   * @returns Promise<void>
   */
  public async register (data: UserCredential): Promise<void> {
    return this.api
      .post(ApiUrl.register, data)
  }

  /**
   * Change one col
   * @param data
   */
  public async patch (data: UserPatch): Promise<UserModel> {
    return this.api
      .patch(ApiUrl.me, data)
      .then(() => {
        return this.getMe()
      })
  }

  /**
   * Change by admin
   * @param data
   */
  public async patchUser (data: UserPatch, id: number): Promise<UserModel> {
    return this.api
      .patch(ApiUrl.user, data, id)
  }

  /**
   * Change one col
   * @param data
   */
  public async delete (id?: number): Promise<void> {
    const idUrl = id === undefined ? ApiUrl.me : ApiUrl.user
    return this.api
      .del(idUrl, id)
  }

  public async getAll (): Promise<UserModel[]> {
    return this.api
      .get(ApiUrl.user)
      .then((resp) => resp.data)
  }

  /**
   * Delete stored data and axios header
   */
  public signOut (): void {
    this.api.delAuth()
    this.storage.del()
  }
}
