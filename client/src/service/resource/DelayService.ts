import { ApiUrl, DelayModel, DelayPut, StationModel, TrainModel } from 'web_trains_types'

import ApiService from '@/service/ApiService'

export default class DelayService {
  constructor (private api: ApiService) {

  }

  public async getAll (): Promise<{delay: DelayModel[], station: StationModel[], train: TrainModel[]}> {
    return this.api.get(ApiUrl.delay)
      .then((resp) => resp.data)
  }

  public async put (data: DelayPut): Promise<number> {
    return this.api.put(ApiUrl.delay, data)
      .then((resp) => resp.data)
  }
}
