import { ApiUrl, JourneyAdd, JourneyModel, JourneyPatch } from 'web_trains_types'

import ApiService from '@/service/ApiService'

export default class JourneyService {
  constructor (private api: ApiService) {

  }

  public async get (id: number): Promise<JourneyModel> {
    return this.api.get(ApiUrl.journey, id)
      .then((resp) => resp.data)
  }

  public async getAll (): Promise<JourneyModel[]> {
    return this.api.get(ApiUrl.journey)
      .then((resp) => resp.data)
  }

  public async add (data: JourneyAdd): Promise<JourneyModel> {
    return this.api.post(ApiUrl.journey, data)
      .then((resp) => resp.data)
  }

  public async patch (id: number, data: JourneyPatch): Promise<void> {
    return this.api.patch(ApiUrl.journey, data, id)
  }

  public async del (id: number): Promise<void> {
    return this.api.del(ApiUrl.journey, id)
  }

  public getLabel (model: JourneyModel): string {
    if (!model.train || !model.stationFrom || !model.stationTo) {
      return model.id.toString()
    }

    return model.train.name + ' z ' + model.stationFrom.name + ' do ' + model.stationTo.name
  }

  public modelToIds (modelAll: JourneyModel[]): number[] {
    return modelAll.map((model) => model.id)
  }
}
