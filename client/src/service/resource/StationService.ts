import { ApiUrl, StationModel } from 'web_trains_types'

import ApiService from '@/service/ApiService'

export default class StationService {
  constructor (private api: ApiService) {

  }

  public async getAll (): Promise<StationModel[]> {
    return this.api.get(ApiUrl.station)
      .then((resp) => resp.data)
  }
}
