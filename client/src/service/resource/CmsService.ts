import { ApiUrl, CmsModel, CmsPatch } from 'web_trains_types'

import { dateToStr } from '@/helper/dateTime'
import ApiService from '@/service/ApiService'

export default class CmsService {
  constructor (private api: ApiService) {

  }

  public async get (): Promise<CmsModel> {
    return this.api.get(ApiUrl.cms, 1)
      .then((response) => {
        const data: CmsModel = response.data

        data.lastChanged = dateToStr(data.lastChanged)

        return data
      })
  }

  public async patch (data: CmsPatch): Promise<CmsModel> {
    return this.api.patch(ApiUrl.cms, data, 1)
  }
}
