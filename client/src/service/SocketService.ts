import { Manager, Socket } from 'socket.io-client'
import { Namespace } from 'web_trains_types'

import { socketError, socketServerError } from '@/exception/ExceptionHandler'
import StorageService from '@/service/StorageService'

export default class SocketService {
  private manager: Manager
  private sockets: Socket[] = []

  constructor (private storage: StorageService) {
    this.manager = new Manager(window.location.origin, {
      path: process.env.VUE_APP_SOCEKT_URL,
      reconnectionDelay: 3000,
      autoConnect: false
    })

    // internal socket error - is logged to server
    this.manager.on('error', socketError)
  }

  /**
   * Create new socket namespace and store it in array
   * @param namespace
   * @param authRequired
   * @returns Socket
   */
  public initSocket (namespace: Namespace, authRequired: boolean): Socket {
    let conf

    if (authRequired) {
      conf = {
        auth: {
          token: this.getJwt()
        }
      }
    }

    const socket = this.manager.socket('/' + namespace, conf)

    // errors received from server
    socket.on('exception', socketServerError)

    // socket.io hack to transfer error data
    socket.on('connect_error', (err: any) => socketServerError(err.data))

    this.sockets.push(socket)

    return socket
  }

  /**
   * Current jwt from storage
   * @returns string|null
   */
  public getJwt (): string|null {
    return this.storage.getJwt()
  }

  /**
   * Disconect all connection
   */
  public disconnect (): void {
    this.sockets.forEach(socket => socket.disconnect())
  }
}
