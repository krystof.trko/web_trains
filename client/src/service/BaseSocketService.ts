import { Socket } from 'socket.io-client'
import { events, eventTransfer, Namespace, Packets } from 'web_trains_types'

import SocketService from '@/service/SocketService'

export default abstract class BaseSocketService {
  private socket: Socket
  protected isAuth: boolean

  constructor (private socketService: SocketService) {
    this.isAuth = this.setAuth()
    this.socket = this.socketService.initSocket(this.setNamespace(), this.isAuth)
  }

  protected abstract setAuth(): boolean;
  protected abstract setNamespace(): Namespace;

  /**
   * Connect to socket
   */
  public connect (): void {
    if (!this.socket.connected) {
      if (this.isAuth) {
        const auth = <{token: string|null}> this.socket.auth
        auth.token = this.socketService.getJwt()
      }

      this.socket.connect()
    }
  }

  /**
   * Emit event and wait to resolve
   * @param event
   * @param data
   * @returns Promise<boolean>
   */
  protected async emit (event: events|eventTransfer, data: Packets): Promise<boolean> {
    return new Promise<boolean>((resolve) => {
      this.socket.emit(event, data, (data: boolean) => {
        resolve(data)
      })
    })
  }

  /**
   * listen to events from server
   * @param event
   * @param callback
   */
  protected on (event: eventTransfer, callback: (...args: any[]) => void): void {
    this.socket.on(event, callback)
  }
}
