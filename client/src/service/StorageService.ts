import { UserModel } from 'web_trains_types'

export default class StorageService {
  private static STORE_JWT = 'user_data_token'
  private static STORE_DATA = 'user_data'

  public getJwt (): string|null {
    return window.localStorage.getItem(StorageService.STORE_JWT)
  }

  public getData (): UserModel|null {
    const str = window.localStorage.getItem(StorageService.STORE_DATA)

    if (str === null) {
      return null
    }

    return JSON.parse(str)
  }

  public del (): void {
    window.localStorage.removeItem(StorageService.STORE_JWT)
    window.localStorage.removeItem(StorageService.STORE_DATA)
  }

  public setJwt (token: string): void {
    window.localStorage.setItem(StorageService.STORE_JWT, token)
  }

  public setData (data: UserModel): void {
    const json = JSON.stringify(data)

    window.localStorage.setItem(StorageService.STORE_DATA, json)
  }
}
