import '@/styles/template.sass'
import '@quasar/extras/material-icons/material-icons.css'

import { createPinia } from 'pinia'
import { Quasar } from 'quasar'
import { createApp } from 'vue'
import VueMatomo from 'vue-matomo'

import App from '@/App.vue'
import consoleImg from '@/config/consoleImg'
import { quasarUserOptions } from '@/config/init'
import { promiseError, vueError, windowError } from '@/exception/ExceptionHandler'
import router from '@/router/router'
import { bootstrapApi, bootstrapSocket } from '@/service'

consoleImg()

const app = createApp(App)

app.config.errorHandler = vueError
window.onerror = windowError
window.addEventListener('unhandledrejection', promiseError)

bootstrapApi()
bootstrapSocket()

app
  .use(createPinia())
  .use(Quasar, quasarUserOptions)
  .use(router)

if (process.env.VUE_APP_ANALYTICS_URL !== false) {
  const matomoConf = {
    host: process.env.VUE_APP_ANALYTICS_URL,
    siteId: 1,
    router: router
  }

  app.use(VueMatomo, matomoConf)
}

app.mount('#app')
