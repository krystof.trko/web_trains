import { ExceptionBody } from 'web_trains_types'

export default class ApiException extends Error {
  constructor (public body: ExceptionBody) {
    super(body.message)
  }
}
