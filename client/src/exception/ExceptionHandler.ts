import { ComponentPublicInstance } from '@vue/runtime-core'
import log4javascript, { AjaxAppender, BrowserConsoleAppender, getLogger, JsonLayout } from 'log4javascript'
import { Notify } from 'quasar'
import { Router } from 'vue-router'
import { ApiUrl, HttpTypes, UnauthorizedTypes } from 'web_trains_types'

import translate, { translateDefault } from '@/config/errorTranslator'
import ApiException from '@/exception/ApiException'
import useUserStore from '@/store/userStore'

// suppress alert
log4javascript.evalInScope('logLog.error = (a, b) => {}')

const loggerAjax = getLogger('ajax')
const ajaxApp = new AjaxAppender(process.env.VUE_APP_API_URL + ApiUrl.error)

ajaxApp.setLayout(new JsonLayout())
loggerAjax.addAppender(ajaxApp)

const loggerConsole = getLogger('console')
const consoleApp = new BrowserConsoleAppender()

loggerConsole.addAppender(consoleApp)

let router: Router|null = null

export function setRouter (r: Router) {
  router = r
}

function notifyApiException (error: ApiException): void {
  if (error.body.code === HttpTypes.Unauthorized && router !== null) {
    const user = useUserStore()
    user.signOut()

    if (error.body.type === UnauthorizedTypes.DeletedUser) {
      router.push({ name: 'home' })
    }

    if (error.body.type === UnauthorizedTypes.TokenExpired) {
      router.push({ name: 'signIn' })
    }
  }

  Notify.create({
    message: error.message,
    type: 'negative'
  })
}

function log (ajaxErr: string, consoleErr: Error|PromiseRejectionEvent|undefined|unknown) {
  loggerAjax.error(ajaxErr)

  if (process.env.NODE_ENV === 'development') {
    loggerConsole.error(consoleErr)
  }
}

export function windowError (message: string | Event, source: string | undefined, lineno: number | undefined, colno: number | undefined, error: Error | undefined): void {
  log(
    'message: ' + message + ' source: ' + source + ' lineno: ' + lineno + ' colno: ' + colno + ' error: ' + error,
    error
  )
}

export function vueError (error: unknown | undefined, vm: ComponentPublicInstance | null | undefined, info: string | undefined): void {
  log(
    'info: ' + info + ' error: ' + error,
    error
  )
}

export function axiosError (error: any): Promise<any> {
  const body = translate(error.response)
  const newErr = new ApiException(body)

  notifyApiException(newErr)

  return Promise.reject(newErr)
}

export function socketError (error: any): void {
  log(
    error,
    error
  )

  Notify.create({
    type: 'negative',
    message: translateDefault
  })
}

export function socketServerError (error: any): void {
  const body = translate(error)

  Notify.create({
    type: 'negative',
    message: body.message
  })
}

export function promiseError (event: PromiseRejectionEvent): void {
  log(
    event.reason,
    event
  )

  Notify.create({
    type: 'negative',
    message: event.reason.message
  })
}
