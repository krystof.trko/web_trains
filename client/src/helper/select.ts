export interface SelectOption {
  value: number,
  label: string
}

export function selectToArray (options: SelectOption[]): number[] {
  return options.map((item) => item.value)
}

export function arrayToSelect (data: number[]|null|undefined, options: SelectOption[]): SelectOption[] {
  if (data === null || data === undefined) {
    return []
  }

  return data.map((item) => {
    const found = options.find((option) => option.value === item)

    if (!found) {
      return {
        value: 0,
        label: ''
      }
    }

    return found
  })
}
