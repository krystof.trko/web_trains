import { format as formatFns, formatISO, parse, parseJSON } from 'date-fns'
import locale from 'date-fns/locale/cs'
import { PlannerTime } from 'web_trains_types'

import { SelectOption } from '@/helper/select'

export function dateToStr (date: string|Date|null|undefined, format = 'PP'): string {
  if (date === null || date === undefined) {
    return ''
  }

  if (typeof date === 'string') {
    date = parseJSON(date)
  }

  return formatFns(date, format, {
    locale: locale
  })
}

// Sunday is 0
export const dayOptions: SelectOption[] = [
  { value: 1, label: 'Pondělí' },
  { value: 2, label: 'Úterý' },
  { value: 3, label: 'Středa' },
  { value: 4, label: 'Čtvrtek' },
  { value: 5, label: 'Pátek' },
  { value: 6, label: 'Sobota' },
  { value: 0, label: 'Neděle' }
]

export const monthOptions: SelectOption[] = [
  { value: 0, label: 'Leden' },
  { value: 1, label: 'Únor' },
  { value: 2, label: 'Březen' },
  { value: 3, label: 'Duben' },
  { value: 4, label: 'Květen' },
  { value: 5, label: 'Červen' },
  { value: 6, label: 'Červenec' },
  { value: 7, label: 'Srpen' },
  { value: 8, label: 'Září' },
  { value: 9, label: 'Říjen' },
  { value: 10, label: 'Listopad' },
  { value: 11, label: 'Prosinec' }
]

export function translateDay (day: number[]|null|undefined): string[] {
  if (day === null || day === undefined) {
    return []
  }

  return day.map((d) => {
    return dayOptions.find((item) => item.value === d)?.label ?? ''
  })
}

export function translateMonth (month: number[]|null|undefined): string[] {
  if (month === null || month === undefined) {
    return []
  }

  return month.map((m) => {
    return monthOptions.find((item) => item.value === m)?.label ?? ''
  })
}

export function translateTime (time: PlannerTime[]|null|undefined): string[] {
  if (time === null || time === undefined) {
    return []
  }

  return time.map((t) => {
    const date = new Date(0, 0, 0, t.h, t.m, 0, 0)

    return formatFns(date, 'p', {
      locale: locale
    })
  })
}

export function encodeTime (time: string[]|null|undefined): PlannerTime[] {
  if (time === null || time === undefined) {
    return []
  }

  return time.map(t => {
    const split = t.split(':')
    return {
      h: parseInt(split[0]),
      m: parseInt(split[1])
    }
  })
}

export function toDatetime (date: string|undefined, time: string|undefined): string|null {
  if (date === undefined || time === undefined) {
    return null
  }

  const parsed = parse(date + ' ' + time, 'yyyy/MM/dd HH:mm', new Date(0, 0, 0, 0, 0, 0, 0))

  return formatISO(parsed)
}
