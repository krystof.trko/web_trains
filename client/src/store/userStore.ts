import { defineStore } from 'pinia'
import { Action, Resource, Role, RolesManager, UserCredential, UserPatch } from 'web_trains_types'

import { adminMapService, socketService, userService } from '@/service'

const roleManager = new RolesManager()

export default defineStore({
  id: 'user',
  state: () => ({
    /** @type UserModel */
    data: {
      id: 0,
      name: '',
      role: ''
    },
    logged: false
  }),
  getters: {
    canAdminUser (): boolean {
      return this.logged && roleManager.can(<Role> this.data.role, Action.crud, Resource.userAdmin)
    },
    canSeeTimetable (): boolean {
      return this.logged && roleManager.can(<Role> this.data.role, Action.see, Resource.timetable)
    },
    canCrudTimetable (): boolean {
      return this.logged && roleManager.can(<Role> this.data.role, Action.crud, Resource.timetable)
    },
    canEditCms (): boolean {
      return this.logged && roleManager.can(<Role> this.data.role, Action.crud, Resource.cms)
    },
    canSeeError (): boolean {
      return this.logged && roleManager.can(<Role> this.data.role, Action.see, Resource.error)
    },
    canCrudError (): boolean {
      return this.logged && roleManager.can(<Role> this.data.role, Action.crud, Resource.error)
    },
    canCrudHwAdmin (): boolean {
      return this.logged && roleManager.can(<Role> this.data.role, Action.crud, Resource.hardwareAdmin)
    }
  },
  actions: {
    init (): void {
      const userData = userService.setAuth()

      if (userData) {
        this.logged = true
        this.data = userData
      }

      if (this.canCrudHwAdmin) {
        adminMapService.connect()
      }
    },
    async patch (data: UserPatch): Promise<void> {
      return userService.patch(data)
        .then((data) => {
          this.data = data
        })
    },
    async delete (): Promise<void> {
      return userService.delete()
    },
    async signIn (user: UserCredential): Promise<void> {
      socketService.disconnect()

      return userService.signIn(user)
        .then((data) => {
          this.data = data
          this.logged = true

          if (this.canCrudHwAdmin) {
            adminMapService.connect()
          }
        })
    },
    async register (data: UserCredential): Promise<void> {
      return userService.register(data)
    },
    signOut (): void {
      socketService.disconnect()

      userService.signOut()
      this.logged = false
      this.data = {
        id: 0,
        name: '',
        role: ''
      }
    }
  }
})
