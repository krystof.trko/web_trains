import { defineStore } from 'pinia'
import { TrainModel } from 'web_trains_types'

import { trainService } from '@/service'

export default defineStore({
  id: 'trainAll',
  state: () => ({
    trainPromise: trainService.getAll({ onlyWithPos: true })
  }),
  getters: {
    trains (): Promise<TrainModel[]> {
      return this.trainPromise
    }
  }
})
