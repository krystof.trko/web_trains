import anime, { AnimeInstance } from 'animejs'

export default class Anim404 {
  c!: HTMLCanvasElement
  ctx!: CanvasRenderingContext2D
  cH!: number
  cW!: number
  bgColor = '#2B4970'
  colors = ['#2B4970', '#748BA7', '#4C688B', '#143054', '#051B38', '#AA6039', '#AA9339', '#FFEEAA', '#D4BF6A', '#806A15', '#554400', '#AA9339', '#AA6039', '#FFC8AA', '#D48F6A', '#803A15', '#551E00']
  colorIndex = 0
  animations: AnimeInstance[] = []

  public init (): void {
    this.c = <HTMLCanvasElement>document.getElementById('canvas404')
    const ctx = this.c.getContext('2d')

    if (ctx === null) {
      return
    }

    this.ctx = ctx

    this.resizeCanvas()
    window.addEventListener('resize', this.resizeCanvas.bind(this))

    this.addClickListeners()

    anime({
      duration: Infinity,
      update: () => {
        this.ctx.fillStyle = this.bgColor
        this.ctx.fillRect(0, 0, this.cW, this.cH)
        this.animations.forEach((anim) => {
          anim.animatables.forEach((animatable) => {
            const target = <Circle><unknown>animatable.target
            target.draw(this.ctx)
          })
        })
      }
    })
  }

  public initClick (): void {
    this.fauxClick(anime.random(this.cW * 0.3, this.cW * 0.7), anime.random(this.cH * 0.8, this.cH * 0.9))

    setTimeout(() => {
      this.initClick()
    }, anime.random(5000, 8000))
  }

  private fauxClick (x: number, y: number): void {
    const fauxClick = new MouseEvent('mousedown', {
      clientX: x,
      clientY: y
    })

    document.dispatchEvent(fauxClick)
  }

  private resizeCanvas (): void {
    this.cW = window.innerWidth
    this.cH = window.innerHeight
    this.c.width = this.cW * devicePixelRatio
    this.c.height = this.cH * devicePixelRatio
    this.ctx.scale(devicePixelRatio, devicePixelRatio)
  }

  private addClickListeners (): void {
    document.addEventListener('touchstart', this.handleEvent.bind(this))
    document.addEventListener('mousedown', this.handleEvent.bind(this))
  }

  private calcPageFillRadius (x: number, y: number): number {
    const l = Math.max(x - 0, this.cW - x)
    const h = Math.max(y - 0, this.cH - y)
    return Math.sqrt(Math.pow(l, 2) + Math.pow(h, 2))
  }

  private handleEvent (e: any): void {
    if (e.touches !== undefined) {
      e.preventDefault()
      e = e.touches[0]
    }

    const currentColor = this.currentColor()
    const nextColor = this.nextColor()
    const targetR = this.calcPageFillRadius(e.pageX, e.pageY)
    const rippleSize = Math.min(200, (this.cW * 0.4))
    const minCoverDuration = 750

    const pageFill = new Circle(
      1, e.pageX, e.pageY, 0, undefined, nextColor
    )

    const fillAnimation = anime({
      targets: pageFill,
      r: targetR,
      duration: Math.max(targetR / 2, minCoverDuration),
      easing: 'easeOutQuart',
      complete: () => {
        this.bgColor = pageFill.fill === undefined ? '#000' : pageFill.fill
        this.removeAnimation(fillAnimation)
      }
    })

    const ripple = new Circle(
      1, e.pageX, e.pageY, 0, {
        width: 3,
        color: currentColor
      }, currentColor
    )

    const rippleAnimation = anime({
      targets: ripple,
      r: rippleSize,
      opacity: 0,
      easing: 'easeOutExpo',
      duration: 900,
      complete: this.removeAnimation
    })

    const particles = []

    for (let i = 0; i < 32; i++) {
      const particle = new Circle(
        1, e.pageX, e.pageY, anime.random(24, 48), undefined, currentColor
      )

      particles.push(particle)
    }

    const particlesAnimation = anime({
      targets: particles,
      x: (particle: any) => {
        return particle.x + anime.random(rippleSize, -rippleSize)
      },
      y: (particle: any) => {
        return particle.y + anime.random(rippleSize * 1.15, -rippleSize * 1.15)
      },
      r: 0,
      easing: 'easeOutExpo',
      duration: anime.random(1000, 1300),
      complete: this.removeAnimation
    })

    this.animations.push(fillAnimation, rippleAnimation, particlesAnimation)
  }

  private removeAnimation (animation: AnimeInstance): void {
    const index = this.animations.indexOf(animation)
    if (index > -1) {
      this.animations.splice(index, 1)
    }
  }

  private nextColor (): string {
    this.colorIndex = this.colorIndex++ < this.colors.length - 1 ? this.colorIndex : 0
    return this.colors[this.colorIndex]
  }

  private currentColor (): string {
    return this.colors[this.colorIndex]
  }
}

class Circle {
  constructor (
    public opacity: number,
    public x: number,
    public y: number,
    public r: number,
    public stroke?: {
      width: number
      color: string
    },
    public fill?: string) {
  }

  public draw (ctx: CanvasRenderingContext2D):void {
    ctx.globalAlpha = this.opacity

    ctx.beginPath()
    ctx.arc(this.x, this.y, this.r, 0, 2 * Math.PI, false)

    if (this.stroke !== undefined) {
      ctx.strokeStyle = this.stroke.color
      ctx.lineWidth = this.stroke.width
      ctx.stroke()
    }

    if (this.fill !== undefined) {
      ctx.fillStyle = this.fill
      ctx.fill()
    }

    ctx.closePath()
    ctx.globalAlpha = 1
  }
}
