/* eslint-disable */
declare module '*.vue' {
  import type { DefineComponent } from 'vue'
  const component: DefineComponent<{}, {}, any>
  export default component
}

declare module 'vue-matomo' {
  import type { Vue } from 'vue'
  export default {
    install: (Vue, options)
  }
}
