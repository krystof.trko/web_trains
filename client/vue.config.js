module.exports = {
  transpileDependencies: [
    'quasar'
  ],
  pluginOptions: {
    quasar: {
      importStrategy: 'kebab',
      rtlSupport: false
    }
  },
  devServer: {
    proxy: {
      '/api': {
        target: 'http://trains_webserver:8081'
      },
      '/socket': {
        target: 'http://trains_webserver:8081'
      },
      '/analytics': {
        target: 'http://trains_matomo:80',
        pathRewrite: { '^/analytics': '' }
      },
      '/adminer': {
        target: 'http://trains_adminer:8080'
      }
    }
  },
  configureWebpack: {
    resolve: {
      symlinks: false
    }
  }
}
